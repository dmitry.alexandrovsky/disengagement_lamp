import adafruit_dotstar as dotstar # pip3 install adafruit-circuitpython-dotstar --break-system-packages
import board
import numpy as np
import animation

class LEDStripe:

    def __init__(self, num_leds = 72, brightness=0.2):
        self.num_leds = num_leds
        self.brightness = brightness
        self.led_state = np.zeros((self.num_leds, 3))
        self.colors = [(0, 0, 255), (0, 0, 255)]
        self.pause_color = (255, 165, 0)
        self.animations = {"idle": None,
                           "pre_start": None, 
                           "post_start": None, 
                           "running": None, 
                           "paused": None, 
                           "pre_end": None, 
                           "post_end": None, 
                           "showcase": None}
        self.animation_settings = {"available_positions": ", ".join(self.animations.keys()),
                            "available_animations" : ", ".join(animation.Animation().get_possibilities()),
                            "idle": None,
                            "pre_start": None, 
                            "post_start": None, 
                            "running": None, 
                            "paused": None, 
                            "pre_end": None, 
                            "post_end": None, 
                            "showcase": None}
        self.animation_state = "idle"
        self.pre_end_threshold = 0.8
        self.scope = None
        self.b_showcase = False
        self.dots = dotstar.DotStar(board.SCK, board.MOSI, self.num_leds, brightness=self.brightness, auto_write=False)

    def set_brightness(self, brightness):
        if brightness is None:
            print("No valid brightness argument:", brightness)
            return
        self.brightness = float(brightness)
        self.dots = dotstar.DotStar(board.SCK, board.MOSI, self.num_leds, brightness=self.brightness)
    
    def get_brightness(self):
        return self.brightness

    def set_color_all(self, color):
        if color is None:
            print("No valid color argument:", color)
            return
        self.color = (int(color[0]), int(color[1]), int(color[2]))
        self.draw_frame(np.tile(self.colors[0], (self.num_leds, 1)))
    
    def get_color(self):
        output_str = ','.join(str(s) for s in self.color)
        return output_str
    
    def get_anim_settings_all(self):
        return self.animation_settings
    
    def draw_frame(self, frame):
        if (np.where(np.any(self.led_state != frame, axis=1))[0]).size > 0:
            for led_id in np.where(np.any(self.led_state != frame, axis=1))[0]:
                self.dots[led_id] = (frame[led_id]).astype(int)
            self.led_state = frame
            self.dots.show()

    def play_anim(self, state, fraction=None):
        frame = None
        match state:
            case "idle":
                # Showcase during or play idle animation
                if self.b_showcase == True:
                    self.animation_state = "showcase"
                    if self.animations[self.animation_state] is not None and self.animations[self.animation_state].get_b_color_assigned() and self.animations[self.animation_state].get_iteration() < 1:
                        frame = self.animations[self.animation_state].next_frame()
                    else:
                        self.b_showcase = False
                        self.animation_state = "idle"
                        if self.animations[self.animation_state] is not None and self.animations[self.animation_state].get_b_color_assigned():
                            frame = self.animations[self.animation_state].next_frame()
                else:
                    self.animation_state = "idle"
                    if self.animations[self.animation_state] is not None and self.animations[self.animation_state].get_b_color_assigned():
                        frame = self.animations[self.animation_state].next_frame()
                    
            case "running":
                # pre_start 
                if self.animation_state == "idle" or (self.animations["pre_start"] is not None and self.animations["pre_start"].get_iteration() < 1):
                    self.animation_state = "pre_start"
                    if self.animations[self.animation_state] is not None and self.animations[self.animation_state].get_b_color_assigned():
                        frame = self.animations[self.animation_state].next_frame()
                    
                # running / post_start / pre_end
                if self.animations["pre_start"] is None or (self.animations["pre_start"] is not None and self.animations["pre_start"].get_iteration() > 0):
                    
                    # running
                    self.animation_state = "running"
                    if self.animations[self.animation_state] is not None and self.animations[self.animation_state].get_b_color_assigned():
                        frame = self.animations[self.animation_state].next_frame(fraction=fraction)
                        self.scope = self.animations[self.animation_state].get_current_scope()
                        
                    # post_start
                    if self.animations["post_start"] is not None and self.animations["post_start"].get_iteration() < 1:
                        self.animation_state = "post_start"
                        if self.animations[self.animation_state] is not None and frame is not None:
                            frame_mod = self.animations[self.animation_state].next_frame(scope=self.scope)
                            frame = frame * frame_mod[:,None]
                    
                    # pre_end
                    if self.animations["pre_end"] is not None and fraction is not None and 1-fraction > self.pre_end_threshold:
                        self.animation_state = "pre_end"
                        if self.animations[self.animation_state] is not None and frame is not None:
                            frame_mod = self.animations[self.animation_state].next_frame(scope=self.scope)
                            frame = frame * frame_mod[:,None]
            
            case "paused":
                    self.animation_state = "paused"
                    if self.animations[self.animation_state] is not None and self.animations[self.animation_state].get_b_color_assigned():
                        frame = self.animations[self.animation_state].next_frame(scope=self.scope)
            
            case "ended":
                    self.animation_state = "post_end"
                    if self.animations[self.animation_state] is not None and self.animations[self.animation_state].get_b_color_assigned():
                        frame = self.animations[self.animation_state].next_frame()
                    if self.animations["pre_start"] is not None and self.animations["pre_start"].get_iteration() > 0:
                        self.animations["pre_start"].reset_iteration()
            
            case "stopped":
                self.animation_state = "post_end"
                if self.animations[self.animation_state] is not None and self.animations[self.animation_state].get_b_color_assigned():
                    frame = self.animations[self.animation_state].next_frame()
                if self.animations["pre_start"] is not None and self.animations["pre_start"].get_iteration() > 0:
                    self.animations["pre_start"].reset_iteration()

        if frame is not None:    
            self.draw_frame(frame)   
        curr_frame, num_frames, anim_name = self.animations[self.animation_state].get_infos()
        return self.animation_state, anim_name, curr_frame, num_frames

    def showcase_animation(self, anim_options, framerate):
        if "anim_pos" in anim_options and anim_options["anim_pos"] != "showcase":
            anim_options["anim_pos"] = "showcase"
        # Flip anim for showcasing
        if "anim_name" in anim_options and anim_options["anim_name"][0:2] == "In":
            anim_options["anim_name"] = "De"  + anim_options["anim_name"][2:]
        elif "anim_name" in anim_options and anim_options["anim_name"][0:2] == "De":
            anim_options["anim_name"] = "In" + anim_options["anim_name"][2:]
        if "anim_name" in anim_options and (anim_options["anim_name"][0:2] == "De" or anim_options["anim_name"][0:2] == "In"):
            col_temp = anim_options["color1"]
            anim_options["color1"] = anim_options["color2"]
            anim_options["color2"] = col_temp
        elif "anim_name" in anim_options and anim_options["anim_name"] == "Pulsation":
            anim_options["color2"] = anim_options["color1"]                                 
                                              
        self.add_animation(anim_options, framerate)
        self.animations["showcase"].reset_frame()
        self.animations["showcase"].reset_iteration()
        self.b_showcase = True

    def add_animation(self, anim_options, framerate):
        # TODO: implement triggering at specific point through triggerpoint, not only for pre end anim
        if "anim_pos" in anim_options and anim_options["anim_pos"] in self.animations and "anim_name" in anim_options and "duration" in anim_options:
            self.animations[anim_options["anim_pos"]] = animation.Animation(anim_name=anim_options["anim_name"], 
                                                        duration=anim_options["duration"],
                                                        num_leds=self.num_leds,
                                                        framerate=framerate,
                                                        num_blocks=anim_options["num_blocks"], settings=anim_options)
            if anim_options["anim_pos"] != "post_start" and anim_options["anim_pos"] != "pre_end" and "color1" in anim_options and "color2" in anim_options and "blend_point" in anim_options:
                self.animations[anim_options["anim_pos"]].set_colorblend(anim_options["color2"], anim_options["color1"], anim_options["blend_point"])
            if "pre_end_threshold" in anim_options:
                self.pre_end_threshold = anim_options["pre_end_threshold"]
            self.animation_settings[anim_options["anim_pos"]] = anim_options
                
            


    



