
from paho.mqtt import client as mqtt_client # pip3 install paho-mqtt==1.6.1 --break-system-packages
import basic_timer
import led_stripe 
import db
import time
import json

#################### 
# TODO: Shift to config file
# General config
update_rate = 30     # [Hz]

# MQTT config
broker = '127.0.0.1' # "pi.local"
port = 1883
client_id_sub = f'sub_pi-1'
client_id_pub = f'pub_pi-1'
topics_sub = [
    "timer/start", "timer/stop", "timer/pause", "timer/set_duration", "timer/modify_duration",
              "light/set_color_all", "light/set_brightness", "light/req_brightness", "light/set_animation", "light/play_animation",
              "light/req_anim_info",
              "stats/req_sessions"
              ]
topics_pub = {"anim_info_topic": "light/anim_info", "light_info_topic": "light/info", "timer_events_topic": "timer/events", "brightness_info_topic": "light/brightness_info",
              "stats_sessions":"stats/sessions"}
####################

database:db.DBHelper = None

# MQTT connect
def connect_mqtt(client_id) -> mqtt_client:
    def on_connect(__, ___, ____, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

# MQTT subscribe
def subscribe_mqtt(client_sub: mqtt_client, topic):
    def on_message(__, ___, msg):
        msg_stack.append(msg)
    client_sub.subscribe(topic)
    client_sub.on_message = on_message

# Message interpretation
def interpret_msg(msg):
    match msg.topic:
        # Timer
        case "timer/start":
            dtimer.start()
            database.create_session_entry(dtimer.duration)
        case "timer/set_duration":
            dtimer.set_duration(json.loads(msg.payload.decode()).get("duration"))
        case "timer/modify_duration":
            dtimer.modify_duration(json.loads(msg.payload.decode()).get("delta"))
            rem_dur, dur, state = dtimer.get_remaining_duration()
            event_info = {"event": "modify_duration", "data": {"delta": json.loads(msg.payload.decode()).get("delta"), "remaining_duration": float(rem_dur), "total_duration": float(dur), "timer_state": state}}
            client_pub.publish(topics_pub["timer_events_topic"], json.dumps(event_info))
            database.update_session_entry(duration_sec=dur)
            print(event_info)
        case "timer/pause":
            dtimer.pause()
        case "timer/stop":
            if dtimer.get_remaining_duration()[-1] == "stopped" or dtimer.get_remaining_duration()[-1] == "ended":
                dtimer.idle()
            elif dtimer.get_remaining_duration()[-1] != "idle":
                dtimer.stop()    

        # Light
        case "light/set_color_all":
            dlight.set_color_all(json.loads(msg.payload.decode()).get("color"))
        case "light/set_brightness":
            dlight.set_brightness(json.loads(msg.payload.decode()).get("brightness"))
        case "light/req_brightness":
            client_pub.publish(topics_pub["brightness_info_topic"], json.dumps({"brightness": dlight.get_brightness()}))
        case "light/set_animation":
            dlight.add_animation(json.loads(msg.payload.decode()), framerate=update_rate)
            client_pub.publish(topics_pub["anim_info_topic"], json.dumps(dlight.get_anim_settings_all()))
        case "light/play_animation":
            dlight.showcase_animation(json.loads(msg.payload.decode()), framerate=update_rate)       
        case "light/req_anim_info":
            client_pub.publish(topics_pub["anim_info_topic"], json.dumps(dlight.get_anim_settings_all()))
        case "stats/req_sessions":
            res = json.dumps([dict(ix) for ix in database.get_all_sessions()])
            client_pub.publish(topics_pub["stats_sessions"], res)

        # Else
        case _:
            print("No matching topic function for", msg.topic)
            print("Message:", msg.payload.decode())



def run():
    state_last = None
    while True:

        # Process messages in stack
        while len(msg_stack) > 0:
            interpret_msg(msg_stack.pop(0))
            
        # Process timer data
        rem_dur, dur, state = dtimer.get_remaining_duration()
        fraction = 0
        if dur > 0:
            fraction = rem_dur/dur            
        curr_anim, anim_choice, fr, num_fr = dlight.play_anim(state, fraction)       

        # Publish timer state changes
        if state_last is None or (state_last is not None and state is not None and state is not state_last):
            client_pub.publish(topics_pub["timer_events_topic"], json.dumps({"event": state, "data": None}))
            state_last = state

        # Publish light info  
        info = {"remaining_duration": float(rem_dur), 
                "total_duration": float(dur), 
                "timer_state": state, 
                "current_animation": curr_anim, 
                "animation_name": anim_choice,
                "current_frame": int(fr), 
                "num_frames": int(num_fr)}
        client_pub.publish(topics_pub["light_info_topic"], json.dumps(info))

        # Roughly aim for the anticipated refresh rate
        time.sleep(1/max((update_rate-1), 1))

if __name__ == '__main__':
    
    # Create timer and led stripe
    dtimer = basic_timer.BasicTimer()
    dlight = led_stripe.LEDStripe()
    database = db.DBHelper()
    database.get_last_entry()

    # Animation presets
    dlight.add_animation({"anim_pos" :"idle", "anim_name": "Pulsation", "duration": 2.0, "color1": (100, 100, 0), "color2": (100, 100, 0), "blend_point": 0.0, "num_blocks": 1}, framerate=update_rate)
    dlight.add_animation({"anim_pos" :"pre_start", "anim_name": "Increasing Middle Line", "duration": 2.0, "color1": (0, 255, 0), "color2": (0, 255, 0), "blend_point": 0.0, "num_blocks": 1}, framerate=update_rate)
    #dlight.add_animation({"anim_pos" :"post_start", "anim_name": "Pulsation", "duration": 2.0, "num_blocks": 1}, framerate=update_rate)
    dlight.add_animation({"anim_pos" :"running", "anim_name": "Decreasing Line", "duration": 144.0, "color1": (0, 255, 0), "color2": (255, 0, 0), "blend_point": 1.0, "num_blocks": 6}, framerate=update_rate)
    dlight.add_animation({"anim_pos" :"pre_end", "anim_name": "Pulsation", "duration": 2.0, "num_blocks": 1}, framerate=update_rate)
    dlight.add_animation({"anim_pos" :"post_end", "anim_name": "Pulsation", "duration": 2.0, "color1": (0, 255, 0), "color2": (255, 0, 0), "blend_point": 0.0, "num_blocks": 1}, framerate=update_rate)
    dlight.add_animation({"anim_pos" :"paused", "anim_name": "Pulsation", "duration": 2.0, "color1": (100, 100, 0), "color2":(100, 100, 0), "blend_point": 0.0, "num_blocks": 1}, framerate=update_rate)
           
    

    # Initialize mqtt clients
    client_sub = connect_mqtt(client_id_sub)
    client_pub = connect_mqtt(client_id_pub)
    for topic in topics_sub:
        subscribe_mqtt(client_sub, topic)
    client_sub.loop_start()
    client_pub.loop_start()
    msg_stack = []

    # Run application
    run()

    # Terminate clients
    client_sub.loop_stop()
    client_pub.loop_stop()
