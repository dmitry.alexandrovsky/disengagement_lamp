import numpy as np

class Animation():

    def __init__(self, anim_name=None, duration=1, num_leds=72, color=None, framerate=10, num_blocks=5, settings={}):
        self.brightness = None
        self.color = None
        self.b_color_assigned = False
        self.num_leds = num_leds
        self.num_frames = None
        self.curr_frame = -1
        self.anim_name = "Custom"
        self.frame = None
        self.framerate = framerate
        self.iteration = 0
        self.num_blocks = num_blocks
        self.block_distance = 3
        self.settings = settings
        self.anim_possibilities = {"Pulsation": self.__sinusoidal,
                                   "Constant View": self.__uniform,
                                   "Increasing Line": self.__linear_increase,
                                   "Decreasing Line": self.__linear_decrease,
                                   "Increasing Blocks": self.__blockwise_increase,
                                   "Decreasing Blocks": self.__blockwise_decrease,
                                   "Increasing Middle Line": self.__grow_outwards}
        

        if anim_name is not None:
            self.set_brightness(anim_name=anim_name, duration=duration)
        if color is not None:
            self.set_color(color)

    def set_brightness(self, brightness=None, anim_name=None, duration=1): 
        if duration < 1/self.framerate:
            duration = 1/self.framerate

        if brightness is not None:
            if self.num_frames is not None and self.num_frames is not np.array(brightness).shape[0]:
                print("Brightness shape [0] does not match num frames:", np.array(brightness).shape[0], "vs.", self.num_frames)
                return
            if np.min(brightness) < 0:
                brightness = brightness - np.min(brightness)
            if np.max(brightness) > 1:
                brightness = brightness / np.max(brightness)
            self.brightness = np.array(brightness)
        elif anim_name is not None and anim_name in self.anim_possibilities:
            if self.num_frames is not None and self.color is not None and np.array(self.color).ndim > 1 and self.num_frames is not duration*self.framerate:
                print("Num frames for this duration does not match num frames of color array:", duration*self.framerate, "vs.", self.num_frames)
                return
            self.anim_name = anim_name
            self.brightness  = self.anim_possibilities[anim_name](duration)          
            self.num_frames = self.brightness.shape[0]


    def set_color(self, color):
        if self.num_frames is not None and np.array(color).ndim > 1 and self.num_frames is not np.array(color).shape[0]:
            print("Color shape [0] does not match num frames:", np.array(color).shape[0], "vs.", self.num_frames)
        elif self.num_frames is not None and np.array(color).ndim == 1:
            self.color = np.array(color)
        else:
            self.color = np.array(color)
            self.num_frames = self.color.shape[0]
    
    def set_colorblend(self, color1, color2, blend_point):
        if self.num_frames is None:
            print("Assign brightness before color blend!")
        elif np.array(color1).ndim != 1 and np.array(color2).ndim != 1:
            print("Color input wrong, expected ndim = 1 but got", np.array(color1).ndim, "/", np.array(color2).ndim)
        elif blend_point is not None and blend_point > 0 and blend_point <=1:
            self.blend_point = blend_point
            r = np.concatenate((np.linspace(color1[0], color2[0], int(self.num_frames*blend_point), endpoint=True),
                               np.linspace(color2[0], color1[0], self.num_frames - int(self.num_frames*blend_point), endpoint=True)))
            g = np.concatenate((np.linspace(color1[1], color2[1], int(self.num_frames*blend_point), endpoint=True),
                                np.linspace(color2[1], color1[1], self.num_frames - int(self.num_frames*blend_point), endpoint=True)))
            b = np.concatenate((np.linspace(color1[2], color2[2], int(self.num_frames*blend_point), endpoint=True),
                                np.linspace(color2[2], color1[2], self.num_frames - int(self.num_frames*blend_point), endpoint=True)))

            self.color = np.column_stack((r, g, b))
            self.b_color_assigned = True
        elif blend_point is not None and blend_point == 0:
            self.set_color(color1)
            self.b_color_assigned = True

    def next_frame(self, fraction = None, scope = None):
        if self.brightness is None:
            print("Animation not initialized yet!")
            return
        if fraction is None:
            if self.curr_frame == self.num_frames -1:
                self.reset_frame()
            self.curr_frame += 1
        elif fraction < 0 or fraction > 1:
            print("Fraction out of bounds [0,1]:", fraction)
            return
        else:
            self.curr_frame = int(self.num_frames * fraction)

        if self.color is None:
            self.frame = self.brightness[self.curr_frame,:]
            if scope is not None:
                self.frame = np.multiply(self.frame, scope)
        else:
            match np.array(self.color).ndim:
                case 1:
                    self.frame = np.reshape(self.brightness[self.curr_frame,:], (self.num_leds, 1)) * np.reshape(self.color, (1,3))
                case 2:
                    self.frame = np.reshape(self.brightness[self.curr_frame,:], (self.num_leds, 1)) * np.reshape(self.color[self.curr_frame], (1,3))
                case 3:
                    self.frame = self.brightness[self.curr_frame,:] * self.color[self.curr_frame,:]
            if scope is not None:
                self.frame = self.frame * scope[:,None]

        return self.frame
    
    def get_infos(self):
        return self.curr_frame, self.num_frames, self.anim_name
    
    def get_settings(self):
        return self.settings
    
    def reset_frame(self):
        self.curr_frame = -1
        self.iteration += 1

    def reset_iteration(self):
        self.iteration = 0

    def get_iteration(self):
        return self.iteration

    def get_current_scope(self):
        return np.sum(self.frame, axis=1) > 0
    
    def get_possibilities(self):
        return list(self.anim_possibilities.keys())
    
    def get_b_color_assigned(self):
        return self.b_color_assigned
        
    def __sinusoidal(self, duration):
        single_led = ((np.sin(np.arange(0, 359, 360/(duration*self.framerate))*np.pi/180) + 1) / 2) * 0.8 + 0.2 
        return np.transpose(np.tile(single_led, (self.num_leds,1))) 
    
    def __uniform(self, duration):
        return np.ones((int(duration*self.framerate), self.num_leds))

    def __linear_decrease(self, duration): 
        lin_tri = np.zeros((int(duration*self.framerate), self.num_leds))
        k = 0
        for i in range(0, int(duration*self.framerate)):
            while i > int(duration*self.framerate *(k-1)/self.num_leds):               
                k += 1
            lin_tri[i,0:k] = 1
        return np.fliplr(lin_tri)

    def __linear_increase(self, duration): 
        return np.fliplr(np.abs(1 - self.__linear_decrease(duration)))

    def __blockwise_increase(self, duration):
        block = np.zeros((int(duration*self.framerate), self.num_leds))
        time_incr = int(np.floor(duration*self.framerate / self.num_blocks))
        led_incr = int(np.floor(self.num_leds / self.num_blocks))
        for i in range(0, self.num_blocks + 1):            
            block[0:time_incr*(i+1),led_incr*(i+1):] = 1
        block[time_incr*self.num_blocks:, :] = 1
        block = self.__draw_block_divider(block)
        return block

    def __blockwise_decrease(self, duration):
        block = np.zeros((int(duration*self.framerate), self.num_leds))
        time_incr = int(np.floor(duration*self.framerate / self.num_blocks))
        led_incr = int(np.floor(self.num_leds / self.num_blocks))
        for i in range(0, self.num_blocks + 1):            
            block[0:time_incr*(i+1),led_incr*i:] = 1
        block[time_incr*self.num_blocks:, :] = 1
        block = self.__draw_block_divider(np.flipud(block))
        return block

    def __draw_block_divider(self, block):
        led_incr = int(np.floor(self.num_leds / self.num_blocks))
        spread = int((self.block_distance - 1) / 2)
        for i in range(1, self.num_blocks + 1):
            block[:, led_incr*i - spread : led_incr*i + spread] = 0
        return block

    def __grow_outwards(self, duration):
        lin_tri = np.zeros((int(duration*self.framerate), int(self.num_leds/2)))
        k = 0
        for i in range(0, int(duration*self.framerate)):
            while i > int(duration*self.framerate) *(k-1)/int(self.num_leds/2):               
                k += 1
            lin_tri[i,0:k] = 1
        return np.concatenate((np.fliplr(lin_tri), np.ones((int(duration*self.framerate), self.num_leds % 2)), lin_tri), axis=1)
