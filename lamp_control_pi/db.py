import sqlite3
from datetime import datetime, timedelta
DB_NAME = 'schlusslicht.db'

class DBHelper:

    db_connection:sqlite3.Connection = None
    db_cursor:sqlite3.Cursor = None
    current_id:int = 0

    def __init__(self) -> None:
        self.init()
        
    def init(self):
        self.db_connection = sqlite3.connect(f"{DB_NAME}")
        self.db_connection.row_factory = sqlite3.Row
        self.db_cursor = self.db_connection.cursor()

        self.db_cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS "session" ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `start_time` TEXT, `end_time` TEXT )
            """)
        

    def get_last_entry(self):
        res = self.db_cursor.execute("SELECT * FROM session ORDER BY id DESC LIMIT 1;")
        entry = res.fetchone()
        print(f"last entry: {entry}")
        if entry is None:
            self.current_id = 0
        else:
            self.current_id = entry[0]
        return entry

    def get_all_sessions(self):
        self.db_cursor.execute("SELECT * FROM session ORDER BY start_time ASC")
        res = self.db_cursor.fetchall()
        self.db_connection.commit()
        return res

    def create_session_entry(self, duration_sec):
        start = datetime.now()
        end = start + timedelta(seconds=duration_sec)
        data = {
            'start_time' : start,
            'end_time': end
        }
        self.db_cursor.execute("INSERT INTO session (start_time, end_time) VALUES(:start_time, :end_time)", data)
        self.db_connection.commit()
        self.get_last_entry()

    def update_session_entry(self, duration_sec):
        id, start, end = self.get_last_entry()
        start = datetime.fromisoformat(start)
        end = start + timedelta(seconds=duration_sec)
        # output = end.strftime('YYYY-MM-DD HH:MM:SS.mmmmmm')
        
        query = f"""
            UPDATE session
            SET end_time = "{end}"
            WHERE
                id = {id}
        """
        print(query)
        self.db_cursor.execute(query)
        
        self.db_connection.commit()

