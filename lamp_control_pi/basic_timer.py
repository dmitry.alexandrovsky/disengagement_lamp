import time

class BasicTimer:
    """Class BaiscTimer: Provide simple timing functionalities.
    """    

    def __init__(self, duration = 15):
        """Create DisengagementTimer instance.

        Args:
            duration (num, optional): Timer duration [s]. Defaults to 15.
        """        
        self.state = "idle"
        self.duration = duration            # [s]
        self.rem_duration = duration        # [s]
        self.start_ref = 0

    def set_duration(self, duration):
        """Set new duration.

        Args:
            duration (num): New timer duration [s].
        """
        if duration is None:
            print("No valid duration argument:", duration)
            return
      
        self.duration = duration
        self.rem_duration = duration
        print("Set duration to", duration)

    def modify_duration(self, delta):
        """Modify duration.

        Args:
            delta (num): Delta for duration [s].
        """
        if delta is None:
            print("No valid delta argument:", delta)
            return
        
        self.rem_duration = self.rem_duration + delta
        self.duration = self.duration + delta
        print("Modified duration in runtime to", self.duration, "( delta:", delta, ")")
        
    def start(self):
        """Start timer.
        """        
        self.start_ref = time.perf_counter()
        self.state = "running"
        print("Started timer, duration:", self.rem_duration, "/", self.duration)

    def pause(self):
        """Pause timer.
        """        
        self.rem_duration = self.get_remaining_duration()[0]
        self.state = "paused"
        print("Paused timer, remaining duration:", self.rem_duration, "/", self.duration)
    
    def stop(self):
        """Stop timer.
        """        
        self.state = "stopped"
        self.rem_duration = 0
        print("Stopped timer")

    def end(self):
        """End timer.
        """        
        self.state = "ended"
        self.rem_duration = 0
        print("Ended timer")

    def idle(self):
        """Set timer to idle state.
        """        
        self.state = "idle"
        self.rem_duration = self.duration
        print("Timer is idle again")
    
    def get_remaining_duration(self):
        """Check remaining duration. This function should be called regularly as it is the only possibility the timer checks whether it is done.

        Returns:
            rem_dur (num): Remaining timer duration in [s].
            duration (num): Total timer duration in [s].
            state (string): Timer state ("idle" / "running" / "paused" / "ended"),
        """        
        if self.state == "running" and self.rem_duration - (time.perf_counter() - self.start_ref) < 0:
            self.end()
        if self.state == "running":
            return self.rem_duration - (time.perf_counter() - self.start_ref), self.duration, self.state
        else:
            return self.rem_duration, self.duration, self.state
