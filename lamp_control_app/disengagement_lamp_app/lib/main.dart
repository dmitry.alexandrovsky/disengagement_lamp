import 'package:disengagement_lamp_app/pages/init_setup_page.dart';
import 'package:disengagement_lamp_app/pages/main_page.dart';
import 'package:disengagement_lamp_app/services/bluetooth_controller.dart';
import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:disengagement_lamp_app/services/notification_controller.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';




void main() {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  // FlutterBluePlus.setLogLevel(LogLevel.verbose, color: true);
  runApp(const DisengagementLampApp());
}


class DisengagementLampApp extends StatefulWidget {
  const DisengagementLampApp({super.key});
  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  static const String name = 'Schlusslicht';
  static const Color mainColor = Color.fromARGB(255, 58, 118, 183);

  @override
  State<DisengagementLampApp> createState() => _DisengagementLampAppState();
}

class _DisengagementLampAppState extends State<DisengagementLampApp> {

  SharedPreferences? sharedPreferences;
  MQTTController? mqttController;
  BluetoothController? bluetoothController;

  
  void _initMQTTController(){
    mqttController = MQTTController( sharedPreferences:sharedPreferences);
    mqttController?.createAndStartAutoConnectTimer();
  }

  void _initBluetoothController(){
    bluetoothController = BluetoothController();
  }

  Future<void> _asyncInit() async {
    sharedPreferences = await SharedPreferences.getInstance();
    await NotificationController.initNotifications();
  }


  @override
  void initState() {
    super.initState();
    _initMQTTController();
    _initBluetoothController();
    _asyncInit();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Disengagement Lamp',
      color: DisengagementLampApp.mainColor,      
      theme: ThemeData(        
        colorScheme: ColorScheme.fromSeed(seedColor: DisengagementLampApp.mainColor), // 
        useMaterial3: true,
      ),
      initialRoute: '/init',
      routes: {
        '/main': (context) => MainPage(
          bluetoothController: bluetoothController,
          mqttController: mqttController,
        ),
        '/init': (context) => InitSetupPage(
          bluetoothController: bluetoothController,
          mqttController: mqttController,
          sharedPreferences: sharedPreferences,
        ),
        
      },
      
      debugShowCheckedModeBanner: false,
      // home: const MainPage(),
    );
  }
}

