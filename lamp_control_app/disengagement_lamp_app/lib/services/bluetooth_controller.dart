import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:disengagement_lamp_app/services/app_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';


class BluetoothDeviceNetworkInfo with ChangeNotifier {
  String ssid = '';
  String ipAddress = '';
  BluetoothDeviceNetworkInfo();
  
  void update(String ssid, String ipAddress){
    this.ssid = ssid;
    this.ipAddress = ipAddress;
    notifyListeners();
  }
}


class BluetoothController {
    static final Guid LAMP_BLE_DEVICE = Guid('00000000-cffb-4231-ad87-db62e82a8017');
    static final Guid LAMP_BLE_CHARACTERISTIC_IP = Guid('00000001-cffb-4231-ad87-db62e82a8017');
    static final Guid LAMP_BLE_CHARACTERISTIC_SSID = Guid('00000003-cffb-4231-ad87-db62e82a8017');
    static final Guid LAMP_BLE_CHARACTERISTIC_PASSWORD = Guid('00000005-cffb-4231-ad87-db62e82a8017');

    BluetoothDevice? device;
    BluetoothService? lampService;
    BluetoothCharacteristic? ipAddressCharacteristic;
    BluetoothCharacteristic? ssidCharacteristic;
    BluetoothCharacteristic? passwordCharacteristic;

    String ssid = 'NONE';
    String ipAddress = 'NONE';
    
    StreamSubscription<BluetoothConnectionState>? subscription;

    Timer? _readNetworkCharacteristicsTimer;
    SharedPreferences? _sharedPreferences;

    BluetoothDeviceNetworkInfo networkInfo = BluetoothDeviceNetworkInfo();

    Timer _createAndStartReadTimer(String timerId){
      return Timer.periodic(const Duration(seconds: 2), 
      (timer) {
        if(device?.isConnected??false){
          readLampNetworkCharacteristics((ssid, ipAddress) {
            print('$ssid, $ipAddress');
            networkInfo.update(ssid, ipAddress);
            _sharedPreferences?.setString(AppPreferences.PREFS_LAMP_SERVER_SSID, ssid);
            _sharedPreferences?.setString(AppPreferences.PREFS_LAMP_SERVER_IP_ADDRESS, ipAddress);
          });
          print('timer: $timerId'); 
        }else{
          timer.cancel();
        } 
        
      });
    }

    Future<void> connectAndInitialize(BluetoothDevice d) async {
      if(d.isConnected && d == device){
        print('device already connected');
        return;
      }
      dispose();
      device = d;
      subscription = device?.connectionState.listen(
        (BluetoothConnectionState state) async {
          if (state == BluetoothConnectionState.disconnected) {
              // 1. typically, start a periodic timer that tries to 
              //    reconnect, or just call connect() again right now
              // 2. you must always re-discover services after disconnection!
              print("BLE: disconnected reason ${device?.disconnectReason}");
          }
      });
      await device?.connect();
      try {
        await device?.discoverServices().then(
        (services) async {
          lampService = services.where((s) => s.serviceUuid == BluetoothController.LAMP_BLE_DEVICE).first;
          // print(lampService);
          ipAddressCharacteristic = lampService?.characteristics.where((c) => c.characteristicUuid == BluetoothController.LAMP_BLE_CHARACTERISTIC_IP).first;
          ssidCharacteristic = lampService?.characteristics.where((c) => c.characteristicUuid == BluetoothController.LAMP_BLE_CHARACTERISTIC_SSID).first;
          passwordCharacteristic = lampService?.characteristics.where((c) => c.characteristicUuid == BluetoothController.LAMP_BLE_CHARACTERISTIC_PASSWORD).first;
      });
      _sharedPreferences = await SharedPreferences.getInstance();
      _readNetworkCharacteristicsTimer = _createAndStartReadTimer(Random().nextInt(100).toString() );
      } catch (e) {
        print('>>> connectAndInitialize $e');
        dispose();
      }
      
      return;
    }

    Future<void> dispose() async {
      device?.cancelWhenDisconnected(subscription!, delayed:true, next:true);
      _readNetworkCharacteristicsTimer?.cancel();
      await device?.disconnect();
    }

    Future<void> readLampNetworkCharacteristics(Function(String,String) onBluetoothWifiSet) async {
      if(device == null || device!.isDisconnected){
        return;
      }
      print('>>> readLampNetworkCharacteristics');
      
      try {
        var ssidInt = await ssidCharacteristic?.read() ?? [];
        ssid = utf8.decode(ssidInt); 

        var ipAddrInt = await ipAddressCharacteristic?.read() ?? [];
        ipAddress = utf8.decode(ipAddrInt); 
        onBluetoothWifiSet(ssid, ipAddress);

        print('>>> readLampNetworkCharacteristics $ssid $ipAddress');
      } catch (e) {
        print('>>> readLampNetworkCharacteristics $e');
      }
      return;
    }

    Future<void> writeSSIDandPassword(String ssid, String password) async {
      try {
        for (var i = 0; i < 2; i++) {
          await ssidCharacteristic?.write(utf8.encode(ssid));
          await passwordCharacteristic?.write(utf8.encode(password));
          // Future.delayed(const Duration(milliseconds: 500));
        }
        // Future.delayed(const Duration(milliseconds: 1000));
      } catch (e) {
        print('writeSSIDandPassword $e');
      }
  } 
}