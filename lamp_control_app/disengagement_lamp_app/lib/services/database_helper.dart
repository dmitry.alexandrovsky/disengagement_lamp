import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
// import 'package:platform_device_id/platform_device_id.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;


class DatabaseHelper {
  static String dbUserID = "";
  static String dbName = "";


  static sql.Database? database;


  static Future<void> init() async {
    DatabaseHelper.dbUserID = await getUserID();
    DatabaseHelper.dbName = "schlusslicht_${DatabaseHelper.dbUserID}.db";
  }

  static Future<String> getUserID() async {
    String? result = "Test"; // await PlatformDeviceId.getDeviceId;
     print("device id: ${result}");
     dbUserID = result!;
     return dbUserID;
  }


  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE items(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        user_id TEXT,
        start_time TIMESTAMP,
        end_time TIMESTAMP,
        value REAL,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }

  static Future<sql.Database> openDB() async {
    await DatabaseHelper.init();
    return sql.openDatabase(
      dbName,
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
        DatabaseHelper.database = database;
      },
    );
  }

  static Future<int> createItem(String? user_id, String? date, double value) async {
    final db = await DatabaseHelper.openDB();

    final data = {'user_id': user_id, 'date': date, 'value': value};
    final id = await db.insert('items', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<List<Map<String, dynamic>>> getItems() async {
    final db = await DatabaseHelper.openDB();
    return db.query('items', orderBy: "id");
  }

  // Get a single item by id
  //We dont use this method, it is for you if you want it.
    static Future<List<Map<String, dynamic>>> getItem(int id) async {
    final db = await DatabaseHelper.openDB();
    return db.query('items', where: "id = ?", whereArgs: [id], limit: 1);
  }

  // Update an item by id
  static Future<int> updateItem(
      int id, String title, String? descrption) async {
    final db = await DatabaseHelper.openDB();

    final data = {
      'title': title,
      'description': descrption,
      'createdAt': DateTime.now().toString()
    };

    final result =
        await db.update('items', data, where: "id = ?", whereArgs: [id]);
    return result;
  }

  // Delete
  static Future<void> deleteItem(int id) async {
    final db = await DatabaseHelper.openDB();
    try {
      await db.delete("items", where: "id = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint("Something went wrong when deleting an item: $err");
    }
  }

  static Future<String> getDBPath()  async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
      String appDocPath =  appDocDir.path;
      var path = p.join(appDocPath, dbName);
      var uri = p.toUri(path).toString();
      print("path: ${path}");
      print("uri: ${uri}");
      return path;
  }





}