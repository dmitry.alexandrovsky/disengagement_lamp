// import 'dart:async';
// import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:disengagement_lamp_app/components/lamp_animation_settings.dart';
import 'package:disengagement_lamp_app/services/app_preferences.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:shared_preferences/shared_preferences.dart';


class TimerDuration{
  int hours = 0;
  int minutes = 0;
  int seconds = 0;

  TimerDuration({this.hours = 0, this.minutes = 0, this.seconds = 0});
  static int hoursFromTotalSeconds(double durationInSeconds){
    return durationInSeconds.toInt()  ~/ 3600;
  }

  static int minutesFromTotalSeconds(double durationInSeconds){
    return durationInSeconds.toInt() % 3600 ~/ 60;
  }

  static int secondsFromTotalSeconds(double durationInSeconds){
    return durationInSeconds.toInt() % 60;
  }

  static TimerDuration fromSeconds(double durationInSeconds) {
    
    int hours = durationInSeconds.toInt()  ~/ 3600;
    int minutes = durationInSeconds.toInt() % 3600 ~/ 60;
    int seconds = durationInSeconds.toInt() % 60;
    return TimerDuration(hours:hours, minutes:minutes,seconds:seconds);
  }

  int totalSeconds(){
    return hours * 3600 + minutes * 60 + seconds;
  }

  @override
    String toString() {
        String sign = hours < 0 ||  minutes < 0 || seconds < 0? '-' : '';
        String h = hours.abs() <= 0 ? '' : '${hours.abs() < 10 ? '0':''}${hours.abs()}:';
        String m = '${minutes.abs() < 10 ? '0':''}${minutes.abs()}:';
        String s = '${seconds.abs() < 10 ? '0':''}${seconds.abs()}';
        return '$sign $h$m$s';
          
    }
}

class MQTTController {
  // String serverUrl = '192.168.1.101'; // 'pi.local'; //'test.mosquitto.org'; // '192.168.213.156'; //

  final subscriptionTopics = [
    'light/info',
    'light/anim_info',
    'timer/remaining_duration',
    'timer/events',
    'stats/sessions'
  ];


  MqttServerClient? client;
  Function? connectionCallback;
  Function? disconnectCallback;
  Function(String error)? connectFailCallback;
  Function(String topic, String message)? listenCallback;

  SharedPreferences? sharedPreferences;

  MQTTController({this.connectionCallback, this.disconnectCallback, this.listenCallback, this.connectFailCallback, required this.sharedPreferences});

  Timer? _autoConnectTimer;

  bool isConnected(){
    // print('mqtt connection status: ${client?.connectionStatus}');
    return client?.connectionStatus?.state == MqttConnectionState.connected;
  }

  String connectionStatus(){
    return client?.connectionStatus.toString() ?? 'Client not available!';
  }

  Timer? createAndStartAutoConnectTimer(){
    _autoConnectTimer = Timer.periodic(
      const Duration(seconds: 5), 
      (timer) {
        // print(">>>>autoConnectTimer");
        if(!isConnected()) {
          print(">>>> TRY TO RECONNECT!!!");
          SharedPreferences.getInstance().then((prefs) {
            String serverUrl = prefs.getString(AppPreferences.PREFS_LAMP_SERVER_IP_ADDRESS) ?? '';
            connect(url: serverUrl);
          });
        }else {
        }
      }
    );
    return _autoConnectTimer;
  }

  void connect({required String url}) async {
    client = MqttServerClient(url, '');
    client?.logging(on: false);
    client?.setProtocolV311();
    client?.keepAlivePeriod = 20;
    client?.connectTimeoutPeriod = 2000; // milliseconds
    
    client?.onConnected = onConnected;
    client?.onDisconnected = onDisconnected;
    client?.onSubscribed = onSubscribed;
    

    try {
      await client?.connect().then(
        (status) {
          if(status?.state == MqttConnectionState.connected) {
            if(connectionCallback != null) {
              connectionCallback!();
            }
          }
        }
      );
      for (var topic in subscriptionTopics) {
        client?.subscribe(topic, MqttQos.atLeastOnce);
      }

      client?.updates!.listen(mqttUpdatesListener);
      
    } catch (e) {
      print(e);
      if(connectFailCallback != null){
        connectFailCallback!(e.toString());
      }
    }
  }

  void mqttUpdatesListener(events) { 
    // print(events);
    for (var event in events) {
      // print('>>>>> ${event.topic}');            
      final body = MqttPublishPayload.bytesToStringAsString((event.payload as MqttPublishMessage).payload.message);
      // print('>>>>>> ${body}');
      if(listenCallback != null){
        // print(event.topic);
        listenCallback!(event.topic, body);
      }
      
    }
  }

  void disconnect(){
    client?.disconnect();
  }

  void dispose(){
    disconnect();
    _autoConnectTimer?.cancel();
  }

  void onConnected() {
    print("on connected");
    // if(connectionCallback != null) {
    //   connectionCallback!();
    // }
  }
  void onDisconnected(){
    print("on disconnected");
    if(disconnectCallback != null) {
      disconnectCallback!();
    }
  }

  void onSubscribed(String topic) {
    print('Subscription confirmed for topic $topic');
  }


void safePublish(String topic, MqttClientPayloadBuilder builder){
  if(isConnected()) {
    try {
      client?.publishMessage(topic, MqttQos.exactlyOnce, builder.payload!);    
    } catch (e) {
      print('error: $e');
    }
  }
}

///// PUBLISH MESSAGES /////
  void publishLightSetColorAll(int red, int green, int blue){
    final builder = MqttClientPayloadBuilder();
    builder.addString('$red,$green,$blue');
    // builder.addInt(red);
    // builder.addInt(green);
    // builder.addInt(blue);
    print('set colors connected: ${isConnected()}');
    safePublish('light/set_color_all', builder);
    // if(isConnected()) {
    //   client?.publishMessage('light/set_color_all', MqttQos.exactlyOnce, builder.payload!);    
    // }
  }

  void publishLightSetAnimation({required LampAnimationSettings animationSettings}) {
    final settings = json.encode({
      'anim_pos': animationSettings.animPos,
      'anim_name' : animationSettings.animName,
      'duration' : animationSettings.duration,
      'blend_point': animationSettings.blendPoint,
      'color1': [animationSettings.colors.first.red, animationSettings.colors.first.green, animationSettings.colors.first.blue],
      'color2': [animationSettings.colors.last.red, animationSettings.colors.last.green, animationSettings.colors.last.blue],
      'num_blocks': animationSettings.numBlocks
    });
    
    final builder = MqttClientPayloadBuilder();
    
    builder.addString(settings.toString());
    safePublish('light/set_animation', builder);
    // if(isConnected()) {
    //   client?.publishMessage('light/set_animation', MqttQos.exactlyOnce, builder.payload!);    
    // }
    animationSettings.update();
  }

  void publishLightPlayAnimation(LampAnimationSettings animationSettings){
    final builder = MqttClientPayloadBuilder();
    final settings = json.encode({
      'anim_pos': animationSettings.animPos,
      'anim_name' : animationSettings.animName,
      'duration' : animationSettings.duration,
      'blend_point': animationSettings.blendPoint,
      'color1': animationSettings.colors.isNotEmpty ? [animationSettings.colors.first.red, animationSettings.colors.first.green, animationSettings.colors.first.blue] : [0,0,0],
      'color2': animationSettings.colors.length > 1 ? [animationSettings.colors.last.red, animationSettings.colors.last.green, animationSettings.colors.last.blue] : [0,0,0],
      'num_blocks': animationSettings.numBlocks
    });
    builder.addString(settings.toString());
    safePublish('light/play_animation', builder);
  }

  void publishLightBrightness(double brightness){
    final builder = MqttClientPayloadBuilder();
    var msg = json.encode({
      'brightness': brightness
    });
    builder.addString(msg.toString());
    client?.publishMessage('light/set_brightness', MqttQos.exactlyOnce, builder.payload!);    
  }

  void publishSetTimerDuration(TimerDuration duration){
    print("publish set timer duration");
    int totalSeconds = duration.hours * 3600 + duration.minutes * 60 + duration.seconds;
    final builder = MqttClientPayloadBuilder();
    builder.addString(jsonEncode({'duration': totalSeconds}));
    safePublish('timer/set_duration', builder);
    // client?.publishMessage('timer/set_duration', MqttQos.exactlyOnce, builder.payload!);    
  }

  void publishTimerStart(){
    final builder = MqttClientPayloadBuilder();
    builder.addString('start');
    safePublish('timer/start', builder);
    // client?.publishMessage('timer/start', MqttQos.atLeastOnce, builder.payload!);
  }

  void publishTimerStop(){
    final builder = MqttClientPayloadBuilder();
    builder.addString('stop');
    safePublish('timer/stop', builder);
    // client?.publishMessage('timer/stop', MqttQos.atLeastOnce, builder.payload!);
  }

  void publishTimerPause(){
    final builder = MqttClientPayloadBuilder();
    builder.addString('pause');
    safePublish('timer/pause', builder);
    // client?.publishMessage('timer/pause', MqttQos.atLeastOnce, builder.payload!);
  }

  void publishTimerModifyDuration(TimerDuration duration){
    print("publish modify timer duration");
    int totalSeconds = duration.hours * 3600 + duration.minutes * 60 + duration.seconds;
    final builder = MqttClientPayloadBuilder();
    var msg = json.encode({
      'delta': totalSeconds
    });    
    builder.addString(msg.toString());
    safePublish('timer/modify_duration', builder);
    // client?.publishMessage('timer/modify_duration', MqttQos.exactlyOnce, builder.payload!);    
  }

  void publishRequestSessionStats(){
    final builder = MqttClientPayloadBuilder();
    var msg = json.encode({
      'none': 'none'
    });    
    builder.addString(msg.toString());
    safePublish('stats/req_sessions', builder);
  }

  void publishMessage(String topic, String message){
    final builder = MqttClientPayloadBuilder();    
    builder.addString(message);
    safePublish(topic, builder);
    // client?.publishMessage(topic, MqttQos.atLeastOnce, builder.payload!);    
  }

  

}