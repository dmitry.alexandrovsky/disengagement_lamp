class AppPreferences {
  static const String PREFS_LAMP_SERVER_IP_ADDRESS = 'PREFS_LAMP_SERVER_IP_ADDRESS';
  static const String PREFS_LAMP_SERVER_SSID = 'PREFS_LAMP_SERVER_SSID';
  static const String APP_DEVICE_IP_ADDRESS = 'APP_DEVICE_IP_ADDRESS';
  static const String APP_DEVICE_SSID = 'APP_DEVICE_SSID';
}