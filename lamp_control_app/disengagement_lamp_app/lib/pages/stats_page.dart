import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';


class SessionStats{
  int id;
  DateTime start;
  DateTime end;
  
  SessionStats({required this.id, required this.start, required this.end});
  
  @override
  factory SessionStats.fromJson(Map<String,dynamic> json) => SessionStats(
    id: json['id'], 
    start: DateTime.parse(json['start_time']),
    end: DateTime.parse(json['end_time'])
  );
}

class SessionStatsList extends ChangeNotifier{
  List<SessionStats> sessions = [];

  void clear(){
    sessions.clear();
    update();
  }
  void add(SessionStats stats){
    sessions.add(stats);
    update();
  }
  void remove(SessionStats stats){
    sessions.remove(stats);
    update();
  }

  void update(){
    notifyListeners();
  }
}

class StatsPage extends StatefulWidget {
  const StatsPage({super.key, required this.mqttController, required this.sessionStats});
  final MQTTController? mqttController;
  final SessionStatsList sessionStats;
  @override
  State<StatsPage> createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.mqttController?.publishRequestSessionStats();
  }
  @override
  Widget build(BuildContext context) {
    return ListenableBuilder(
      listenable: widget.sessionStats,
        builder: (context, child){
          double meanDurationSec = widget.sessionStats.sessions.map((e) => e.end.difference(e.start)).fold(Duration.zero, (prev, curr) => prev + curr).inSeconds / widget.sessionStats.sessions.length;
          return Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                SfCartesianChart(
                  isTransposed: false,
                  primaryXAxis: DateTimeAxis(
                    axisLabelFormatter: (axisLabelRenderArgs) {
                        final String text = DateFormat('dd.MM.yyyy').format(
                            DateTime.fromMillisecondsSinceEpoch(
                                  axisLabelRenderArgs.value.toInt()));
                        TextStyle style = Theme.of(context).textTheme.bodyMedium!;
                    return ChartAxisLabel(text, style);
                    },
                  ),
                  primaryYAxis: const NumericAxis(),
                  series: <CartesianSeries<SessionStats, DateTime>>[
                    LineSeries(
                      dataSource: widget.sessionStats.sessions,
                      xValueMapper: (SessionStats stats, _) {
                        return stats.start;
                      }, 
                      yValueMapper: (SessionStats stats, _) {
                        return  stats.end.difference(stats.start).inSeconds;
                      })
                  ],
                ),
                
                Row(
                  children: [
                    Text(
                      "Average session duration: ",
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                    Text(
                      "${meanDurationSec.toStringAsFixed(2)} s",
                      style: Theme.of(context).textTheme.bodyLarge,
                      )
                  ],
                ),
                const Spacer(),
                FilledButton(onPressed: (){
                  widget.mqttController?.publishRequestSessionStats();
                }, child: const Icon(Icons.update))
              ],
            ),
          ),
        );
      }
    );
  }
}