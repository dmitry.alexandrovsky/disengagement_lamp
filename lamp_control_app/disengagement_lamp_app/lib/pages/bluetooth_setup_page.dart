import 'dart:async';

import 'package:disengagement_lamp_app/components/bt2wifi/bt_off.dart';
import 'package:disengagement_lamp_app/components/bt2wifi/bt_scan.dart';

import 'package:disengagement_lamp_app/services/bluetooth_controller.dart';
import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';


class BluetoothSetupPage extends StatefulWidget {
  final MQTTController? mqttController;
  final BluetoothController? bluetoothController;
  const BluetoothSetupPage({super.key, required this.mqttController, required this.bluetoothController});

  @override
  State<BluetoothSetupPage> createState() => _BluetoothSetupPageState();
}

class _BluetoothSetupPageState extends State<BluetoothSetupPage> {
  BluetoothAdapterState _adapterState = BluetoothAdapterState.unknown;
  late StreamSubscription<BluetoothAdapterState> _adapterStateStateSubscription;
  
  @override
  void initState() {
    super.initState();
    _adapterStateStateSubscription = FlutterBluePlus.adapterState.listen((state) {
      setState(() {
        _adapterState = state;  
      });
      print(state);
    });
  }

  @override
  void dispose() {
    _adapterStateStateSubscription.cancel();
    super.dispose();
  }

  void onBluetoothWifiSet(String ssid, String ipAddress) async {
    print('>>>>>> onBluetoothWifiSet $ssid $ipAddress');
  }

  @override
  Widget build(BuildContext context) {
    return _adapterState == BluetoothAdapterState.on
    ? BluetoothScan(onBluetoothWifiSet: onBluetoothWifiSet, bluetoothController: widget.bluetoothController)
    : const BluetoothOff();
  }
}