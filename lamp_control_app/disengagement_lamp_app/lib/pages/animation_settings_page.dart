import 'package:circular_color_picker/circular_color_picker.dart';
import 'package:disengagement_lamp_app/components/lamp_animation_settings.dart';
import 'package:disengagement_lamp_app/components/lamp_not_connected.dart';
import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';



class LampSettingsPage extends StatefulWidget {
  final MQTTController? mqttController;
  final LampAnimationSettingsMap animationSettings;
  const LampSettingsPage({super.key, required this.mqttController, required this.animationSettings});
  @override
  State<LampSettingsPage> createState() => _SettingsPageStateState();
}

class _SettingsPageStateState extends State<LampSettingsPage> {
  
  @override
  void initState() {
    super.initState();
    var topic = 'light/req_anim_info';
    var value = 'none';
    widget.mqttController?.publishMessage(topic, value);
    setState(() {});
  }

  double _brightness = 0.5;

   static const animationNames = <DropdownMenuItem<String>>[
    DropdownMenuItem<String>(
      value: 'Pulsation', 
      child: Text('Pulsation')
    ),
    DropdownMenuItem<String>(
      value: 'Constant View', 
      child: Text('Constant View')
    ),
    DropdownMenuItem<String>(
      value: 'Increasing Line', 
      child: Text('Increasing Line')
    ),
    DropdownMenuItem<String>(
      value: 'Decreasing Line', 
      child: Text('Decreasing Line')
    ),
    DropdownMenuItem<String>(
      value: 'Increasing Blocks', 
      child: Text('Increasing Blocks')
    ),
    DropdownMenuItem<String>(
      value: 'Decreasing Blocks', 
      child: Text('Decreasing Blocks')
    ),
    DropdownMenuItem<String>(
      value: 'Increasing Middle Line', 
      child: Text('Increasing Middle Line')
    ),
  ];
  
  @override
  Widget build(BuildContext context) {
    // print(widget.animationSettings.animationSettings);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListenableBuilder(
        listenable: widget.animationSettings,
        builder: (context, child) {
          if(widget.mqttController != null 
          && widget.mqttController!.isConnected()
          && widget.animationSettings.animationSettings.isNotEmpty
          ) {
            return Center(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Text(
                      'Idle Color',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    // const Spacer(),
                    CircularColorPicker(
                      pickerOptions: CircularColorPickerOptions(
                        initialColor: widget.animationSettings.animationSettings.containsKey('idle')
                        ? widget.animationSettings.animationSettings['idle']!.colors[0]
                        : Colors.white,
                        callOnChangeFunctionOnEnd: false,
                      ),
                      radius: 72,
                      onColorChange: (Color color) {
                        if(widget.animationSettings.animationSettings.containsKey('idle')){
                          setState(() {
                            widget.animationSettings.animationSettings['idle']!.colors[0] = color;
                          });
                          widget.mqttController?.publishLightPlayAnimation(widget.animationSettings.animationSettings['idle']!);
                        }
                      },
                    ),
                    const Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Text(
                              'Start Color',
                              style: Theme.of(context).textTheme.headlineMedium,
                            ),
                            CircularColorPicker(
                              pickerOptions: CircularColorPickerOptions(
                                initialColor: widget.animationSettings.animationSettings.containsKey('running')
                                ? widget.animationSettings.animationSettings['running']!.colors[1]
                                : Colors.white,
                                callOnChangeFunctionOnEnd: false,
                              ),
                              radius: 72,
                              onColorChange: (Color color) {
                                if(widget.animationSettings.animationSettings.containsKey('running')){
                                  setState(() {
                                    widget.animationSettings.animationSettings['running']!.colors[1] = color;
                                  });
                                  widget.mqttController?.publishLightPlayAnimation(widget.animationSettings.animationSettings['running']!);
                                }
                              },
                            ),
                          ],
                        ),                      
                        Column(
                          children: [
                            Text(
                              'End Color',
                              style: Theme.of(context).textTheme.headlineMedium,
                            ),
                            CircularColorPicker(
                              pickerOptions: CircularColorPickerOptions(
                                initialColor: widget.animationSettings.animationSettings.containsKey('running')
                                ? widget.animationSettings.animationSettings['running']!.colors[0]
                                : Colors.white,
                                callOnChangeFunctionOnEnd: false,
                              ),
                              radius: 72,
                              onColorChange: (Color color) {
                                if(widget.animationSettings.animationSettings.containsKey('running')){
                                  setState(() {
                                    widget.animationSettings.animationSettings['running']!.colors[0] = color;
                                  });
                                  widget.mqttController?.publishLightPlayAnimation(widget.animationSettings.animationSettings['running']!);
                                }
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                    const Divider(),
                    Column(
                      children: [
                        Text(
                          'Animation', 
                          style: Theme.of(context).textTheme.headlineMedium,),
                        Row(
                          children: [
                            DropdownButton(
                              value: widget.animationSettings.animationSettings.containsKey('running') 
                              ? widget.animationSettings.animationSettings['running']!.animName
                              : animationNames.first.value,
                              items: animationNames, 
                              onChanged: (String? value){
                                if(widget.animationSettings.animationSettings.containsKey('running') ){
                                  setState(() {
                                    widget.animationSettings.animationSettings['running']!.animName = value!;
                                  });
                                  widget.mqttController?.publishLightPlayAnimation(widget.animationSettings.animationSettings['running']!);
                                }
                              }
                            ),
                            const Spacer(),
                            IconButton(
                              iconSize: 48,
                              icon: const Icon(Icons.play_circle_fill_outlined),
                              onPressed: (){
                                if(widget.animationSettings.animationSettings.containsKey('running') ){
                                  widget.mqttController?.publishLightPlayAnimation(widget.animationSettings.animationSettings['running']!);
                                }
                              },
                            )
                          ],
                        ),
                        const SizedBox(height: 16,),
                        Text(
                          'Duration', 
                          style: Theme.of(context).textTheme.headlineMedium,),
                        NumberPicker(
                          minValue: 5, 
                          maxValue: 180,
                          step: 5,
                          axis: Axis.horizontal, 
                          value: widget.animationSettings.animationSettings.containsKey('running') 
                              ? widget.animationSettings.animationSettings['running']!.duration.toInt()
                              : 10,
                          // value: widget.animationSettings.animationSettings['running']!.duration.toInt(), 
                          onChanged: (value) {
                            if(widget.animationSettings.animationSettings.containsKey('running')){
                              setState(() {
                                widget.animationSettings.animationSettings['running']!.duration = value.toDouble();  
                              });
                              widget.mqttController?.publishLightPlayAnimation(widget.animationSettings.animationSettings['running']!);
                            }
                          }
                        ),
                        const SizedBox(height: 16,),
                        widget.animationSettings.animationSettings['running']!.animName.toLowerCase().contains('block')?
                        Column(
                          children: [
                            Text(
                              'Number of Blocks', 
                              style: Theme.of(context).textTheme.headlineMedium,
                            ),
                            NumberPicker(
                              minValue: 1,
                              maxValue: 12,
                              axis: Axis.horizontal, 
                              value: widget.animationSettings.animationSettings.containsKey('running') ? widget.animationSettings.animationSettings['running']!.numBlocks : 5, 
                              onChanged: (value){
                                if(widget.animationSettings.animationSettings.containsKey('running')){
                                  setState(() {
                                    widget.animationSettings.animationSettings['running']!.numBlocks = value;  
                                  });
                                  widget.mqttController?.publishLightPlayAnimation(widget.animationSettings.animationSettings['running']!);
                                }
                              }
                            )
                          ],
                        )
                        : Container()
                      ],
                    ),
                    const Divider(),
                    Text(
                      'Brightness', 
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    Slider(
                      value: _brightness,
                      min: 0.0,
                      max: 1.0, 
                      onChanged: (value) {
                        setState(() {
                          _brightness = value;  
                        });
                        widget.mqttController?.publishLightBrightness(_brightness);
                      }
                    ),
                    const Divider(),
                    FilledButton(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(Icons.publish, size: 32,),
                          Text(
                            'Update', 
                            style: Theme.of(context).textTheme.headlineSmall,)
                      ],),
                      onPressed: () {
                        if(widget.animationSettings.animationSettings.containsKey('idle')){
                          widget.mqttController?.publishLightSetAnimation(animationSettings: widget.animationSettings.animationSettings['idle']!);
                        }
                        if(widget.animationSettings.animationSettings.containsKey('running')){
                          widget.mqttController?.publishLightSetAnimation(animationSettings: widget.animationSettings.animationSettings['running']!);
                        }
                      }, 
                    )
                  ],
                ),
              ),
            );
                
            
            // return Accordion(
            //   children: [
            //     ...widget.animationSettings.animationSettings.values.map( 
            //       (settings) {
            //         return AccordionSection(
            //           header: Padding(
            //             padding: const EdgeInsets.all(8.0),
            //             child: Text(
            //               settings.animPos, 
            //               style: Theme.of(context).textTheme.headlineMedium,
            //             ),
            //           ), 
            //           content: LampAnimationSettingsWidget(
            //             settings: settings,
            //             mqtt: widget.mqttController,
            //           )
            //         );
            //       }
            //     ),
            //   ],
            // );
          }
          else {
              return const LampDisconnectedWidget();
            }
        }
      )
    );
  }
}