
import 'package:disengagement_lamp_app/components/init_setup/init_setup_finish.dart';
import 'package:disengagement_lamp_app/components/init_setup/init_setup_welcome.dart';
import 'package:disengagement_lamp_app/pages/bluetooth_setup_page.dart';
import 'package:disengagement_lamp_app/services/app_preferences.dart';
import 'package:disengagement_lamp_app/services/bluetooth_controller.dart';
import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppNetworkInfo with ChangeNotifier{
  String wifiName = '';
  String wifiIP = '';

  void update(String wifiName, String ipAddress){
    this.wifiName = wifiName;
    wifiIP = ipAddress;
    notifyListeners();
  }
}

class InitSetupPage extends StatefulWidget {
  final MQTTController? mqttController;
  final BluetoothController? bluetoothController;
  final SharedPreferences? sharedPreferences;
  final AppNetworkInfo networkInfo = AppNetworkInfo();
  InitSetupPage({super.key, required this.mqttController, required this.bluetoothController, required this.sharedPreferences});
  
  
  // String wifiName = '';
  // String wifiIP = '';
  
  @override
  State<InitSetupPage> createState() => _InitSetupPageState();
}

class _InitSetupPageState extends State<InitSetupPage> {
  
  final PageController _pageController = PageController(
      viewportFraction: 1.0,
      initialPage: 0,
  );

  static const _pageAnimDuration = Duration(milliseconds: 300);
  static const _pageAnimCurve = Curves.ease;

  int _currentPageIndex = 0;
  
  List<Widget> _pages = [];

  List<Widget> _buildPages() {
    return <Widget>[
      const InitSetupWelcome(),
      // InitSetupNetwork(appNetworkInfo: widget.networkInfo),
      BluetoothSetupPage(mqttController: widget.mqttController, bluetoothController: widget.bluetoothController),
      InitSetupFinish(mqttController: widget.mqttController)
    ];
  }


  Future<void> _getNetworkInfo() async {
    
    PermissionStatus status = await Permission.location.request();
    print(status);
    if(status.isDenied || status.isPermanentlyDenied){
      const SnackBar(content: Text('Location permission is required'),);
    }

    final networkInfo = NetworkInfo();
    String wifiName = await networkInfo.getWifiName()?? 'test name';
    String wifiIP = await networkInfo.getWifiIP()?? 'test ip';
    
    setState(() {
      widget.networkInfo.update(wifiName, wifiIP);
      
    });
    SharedPreferences.getInstance().then((sp) {
      sp.setString(AppPreferences.APP_DEVICE_IP_ADDRESS, wifiIP);
      sp.setString(AppPreferences.APP_DEVICE_SSID, wifiName);
    });
    
    print('my wifi: $wifiName');
    print('my ip address: $wifiIP');
  }

  @override
  void initState() {
    print('init setup page');
    _pages = _buildPages();
    super.initState();
    widget.mqttController?.connectionCallback = () {
      Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
    };
    widget.mqttController?.connectFailCallback = (error) {};
    _getNetworkInfo();
    FlutterNativeSplash.remove();


  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Schlusslicht Setup'),
        actions: [
          widget.mqttController != null ?
          widget.mqttController!.isConnected() ? const Icon(Icons.circle, color: Color.fromARGB(183, 50, 184, 72),) : const Icon(Icons.circle, color: Color.fromARGB(255, 255, 50, 36),) 
          :  const Icon(Icons.circle, color: Color.fromARGB(255, 174, 215, 10),),
        ],
        ),
      body: Column(
        children: [
          Flexible(
            child: PageView(
              onPageChanged: (page){
                setState(() {
                  _currentPageIndex = page;
                });
                print('current page: $_currentPageIndex');
              },
              controller: _pageController,
              children: _pages,
            ),
          ),
          Container(
            // color: Colors.lightBlueAccent,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FilledButton(
                  onPressed: _currentPageIndex > 0 ? () {
                    _pageController.previousPage(
                        duration: _pageAnimDuration, curve: _pageAnimCurve);
                        
                  }: null,
                  child: const Text('Prev'),
                ),
                FilledButton(
                  onPressed: _currentPageIndex < _pages.length-1 ? () {
                    _pageController.nextPage(duration: _pageAnimDuration, curve: _pageAnimCurve);
                  }: null,
                  child: const Text('Next'),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}