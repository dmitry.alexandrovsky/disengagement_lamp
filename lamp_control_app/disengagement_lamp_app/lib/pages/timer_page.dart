import 'package:disengagement_lamp_app/components/lamp_timer.dart';
import 'package:disengagement_lamp_app/components/lamp_timer_controls.dart';
import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:flutter/material.dart';

class TimerPage extends StatefulWidget {
  final MQTTController? mqttController;
  final LampTimerSettings timerSettings;
  const TimerPage({super.key, required this.mqttController, required this.timerSettings});

  @override
  State<TimerPage> createState() => _TimerPageState();
}

class _TimerPageState extends State<TimerPage> {
  @override
  Widget build(BuildContext context) {
    LampTimerWidget timerWidget = LampTimerWidget(mqttController: widget.mqttController, settings: widget.timerSettings);
    return Column(
      children: [
        // ProgressiveTimerWidget(mqttController: widget.mqttController, settings: widget.timerSettings),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: timerWidget
        ),
        LampTimerControlsWidget(mqttController: widget.mqttController, settings: widget.timerSettings, timerWidget: timerWidget,)
      ],
    );
  }
}
