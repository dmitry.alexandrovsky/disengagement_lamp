import 'dart:convert';

import 'package:disengagement_lamp_app/components/lamp_animation_settings.dart';
import 'package:disengagement_lamp_app/components/lamp_timer.dart';
import 'package:disengagement_lamp_app/pages/bluetooth_setup_page.dart';
import 'package:disengagement_lamp_app/pages/animation_settings_page.dart';
import 'package:disengagement_lamp_app/pages/stats_page.dart';
import 'package:disengagement_lamp_app/pages/timer_page.dart';

import 'package:disengagement_lamp_app/services/bluetooth_controller.dart';
import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:disengagement_lamp_app/services/notification_controller.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:shared_preferences/shared_preferences.dart';



class MainPage extends StatefulWidget {
  final MQTTController? mqttController;
  final BluetoothController? bluetoothController;
  const MainPage({super.key, required this.mqttController, required this.bluetoothController});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  SharedPreferences? _sharedPreferences;
  // Page Navigation
  int _currentPageIndex = 0;
  final SessionStatsList _sessionStats = SessionStatsList();
  List<Widget> _pages = [
    const Placeholder(color: Colors.red),
  ];

  List<Widget> _initPages(MQTTController? mqtt, BluetoothController? bluetoothController, LampTimerSettings timerSettings, LampAnimationSettingsMap animationSettings){
    return [
      TimerPage(mqttController: mqtt, timerSettings: timerSettings),
      LampSettingsPage(mqttController: mqtt, animationSettings: animationSettings),
      BluetoothSetupPage(mqttController: mqtt, bluetoothController: bluetoothController),
      StatsPage(mqttController: mqtt, sessionStats: _sessionStats),
    ];
  }

  final LampTimerSettings _timerSettings = LampTimerSettings();
  


  // Animation
  final LampAnimationSettingsMap _animationSettings = LampAnimationSettingsMap();



  void _handleTimerEvents(dynamic messageJson){
    switch(messageJson['event']){
      case 'idle':
      break;
      case 'running':
      
        NotificationController.createScheduledNotification(DateTime.now().add(Duration(seconds: _timerSettings.timerDuration.totalSeconds())));
      break;
      case 'paused':
        NotificationController.cancelScheduledTimerNotifications();
      break;
      case 'ended':
        HapticFeedback.vibrate();
        SystemSound.play(SystemSoundType.alert);
      // NotificationController.createTimerEndedNotification();
      break;
      case 'stopped':
      break;
      case 'modify_duration':
      print('----on modified timer $messageJson');
      NotificationController.cancelScheduledTimerNotifications();
      // NotificationController.createScheduledNotification(DateTime.now().add(Duration(seconds: _timerSettings.timerDuration.totalSeconds())));
      break;
    }
  }

  void _mqttListenCallback(String topic, String message){
    final jsonMsg = jsonDecode(message);
    switch(topic) {
      case 'light/info':
        setState(() {
          final remainingTimerDuration = jsonMsg['remaining_duration'];
          final totalTimerDuration = jsonMsg['total_duration'];
          final timerState = jsonMsg['timer_state'];
          _timerSettings.update(TimerDuration.fromSeconds(remainingTimerDuration), remainingTimerDuration, totalTimerDuration, timerState);
        });
      break;
      case 'light/anim_info':
        List<String> availablePositions = jsonMsg['available_positions'].split(', ');
        List<String> availableAnimations = jsonMsg['available_animations'].split(', ');
        // print(availableAnimations);
        for(var pos in availablePositions) {
          // print(pos);
          // print(jsonMsg[pos]);
          // print(availableAnimations);
          if(pos == "showcase" || pos == 'post_start' ) {
            continue;
          }
          _animationSettings.update(pos, LampAnimationSettings.fromJson(jsonMsg[pos], availableAnimations));
        }
      break;
        case 'timer/events':
        _handleTimerEvents(jsonMsg);
      break;
      case 'stats/sessions':
        _sessionStats.clear();
        for(var row in jsonMsg){
          _sessionStats.add(SessionStats.fromJson(row));
        }
        break;      
    }
  }

  void _initMQTTController(){
    widget.mqttController?.listenCallback = _mqttListenCallback;
  }

  

  @override
  void initState() {
    super.initState();
    _initMQTTController();
     SharedPreferences.getInstance().then((value) => _sharedPreferences);
    setState(() {
      _pages = _initPages(widget.mqttController, widget.bluetoothController, _timerSettings, _animationSettings);  
    });
    FlutterNativeSplash.remove();
  }

  @override
  void dispose() {
    widget.mqttController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Schlusslicht'),
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        actions: [
          widget.mqttController != null ?
          widget.mqttController!.isConnected() ? const Icon(Icons.circle, color: Color.fromARGB(183, 50, 184, 72),) : const Icon(Icons.circle, color: Color.fromARGB(255, 255, 50, 36),) 
          :  const Icon(Icons.circle, color: Color.fromARGB(255, 174, 215, 10),),
          // _mqttController!.isConnected() ? const Icon(Icons.circle, color: Color.fromARGB(183, 50, 184, 72),) : const Icon(Icons.circle, color: Color.fromARGB(255, 255, 50, 36),),
        ],
      ),
      bottomNavigationBar: NavigationBar(
        selectedIndex: _currentPageIndex,
        onDestinationSelected: (int index) {
          setState(() {
            _currentPageIndex = index;
          });
        },
        destinations: const <Widget>[
          NavigationDestination(icon: Icon(Icons.timer), label: 'Timer',),
          NavigationDestination(icon: Icon(Icons.lightbulb), label: 'Light Settings',),
          NavigationDestination(icon: Icon(Icons.settings), label: 'Settings',),
          NavigationDestination(icon: Icon(Icons.bar_chart), label: 'Stats',),
        ],
      ),
      body: SafeArea(
        child: _pages[_currentPageIndex]
      )      
    );
  }
}