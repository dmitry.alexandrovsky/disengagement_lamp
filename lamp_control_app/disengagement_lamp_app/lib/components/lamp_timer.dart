// ignore_for_file: must_be_immutable

import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';


class LampTimerSettings with ChangeNotifier{
  DateTime startTime = DateTime.now();
  TimerDuration timerDuration = TimerDuration(hours: 0, minutes: 5, seconds: 0);
  double remainingTimerDuration = 0.0;
  double totalTimerDuration = 0.0;
  String timerState = 'idle';
  
  void update(TimerDuration timerDuration, double remainingTimerDuration, double totalTimerDuration, String timerState){
    this.timerDuration = timerDuration;
    this.remainingTimerDuration = remainingTimerDuration;
    this.totalTimerDuration = totalTimerDuration;
    this.timerState = timerState;

    notifyListeners();
  }
}

class LampTimerWidget extends StatefulWidget {
  
  final MQTTController? mqttController;
  final LampTimerSettings settings;
  LampTimerWidget({super.key, required this.mqttController, required this.settings});
  
  bool isCoolDown = false;
  final int coolDownDurationMilliSec = 500;

  void startCoolDown() async {
    isCoolDown = true;
    await Future.delayed(Duration(milliseconds: coolDownDurationMilliSec));
    isCoolDown = false;
  }

  @override
  State<LampTimerWidget> createState() => _LampTimerWidgetState();
  
}


class _LampTimerWidgetState extends State<LampTimerWidget> {
  
  @override
  Widget build(BuildContext context) {
    
    return ListenableBuilder(
      listenable: widget.settings,
      builder: (context, child) {
        
        return Container(
            // shape: RoundedRectangleBorder(
            //   borderRadius: BorderRadius.all(Radius.circular(32)),
            //   side: BorderSide(color: Theme.of(context).colorScheme.secondary)
            // ),
            child: AnimatedSwitcher(
              duration: const Duration(seconds: 1),
              transitionBuilder: (child,animation) => ScaleTransition(scale: animation, child:child),
              child: 
              // timerState == 'running'? Container(
              //     child: Text(
              //         formatHHMMSS(remainingDuration.toInt()),
              //         style: Theme.of(context).textTheme.displayLarge,
              //       ),
              //   ) : 
                Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    // Text(
                    //   "Hours",
                    //   style: Theme.of(context).textTheme.headlineSmall
                    // ),
                    
                    IgnorePointer(
                      ignoring: widget.settings.timerState == 'running',
                      child: NumberPicker(// Hours
                        minValue: 0, 
                        maxValue: 24, 
                        value: widget.settings.timerDuration.hours,
                        infiniteLoop: true,
                        zeroPad: true,
                        onChanged: (int i){
                          if(!widget.isCoolDown){
                            setState(() {
                              widget.settings.timerDuration.hours = i;
                            });
                            if(widget.settings.timerState != 'running') {
                              widget.mqttController?.publishSetTimerDuration(widget.settings.timerDuration);
                            }
                          }
                        }
                      ),
                    )
                    
                  ],
                ),
                Text(":",style: Theme.of(context).textTheme.headlineLarge,),
                Column(
                  children: [
                    // Text(
                    //   "Minutes",
                    //   style: Theme.of(context).textTheme.headlineSmall
                    // ),
                    IgnorePointer(
                      ignoring: widget.settings.timerState == 'running',
                      child: NumberPicker(// Minutes
                      minValue: 0, 
                      maxValue: 60, 
                      value: widget.settings.timerDuration.minutes,
                      step: 1,
                      zeroPad: true,
                      infiniteLoop: true, 
                      onChanged: (int i){
                        if(!widget.isCoolDown){
                          setState(() {
                            widget.settings.timerDuration.minutes = i;
                          });
                          if(widget.settings.timerState != 'running'){
                            widget.mqttController?.publishSetTimerDuration(widget.settings.timerDuration);
                          }
                        }
                      }
                    ),
                  )                    
                ],
              ),
              Text(":",style: Theme.of(context).textTheme.headlineLarge,),
                Column(
                  children: [
                    // Text(
                    //   "Seconds",
                    //   style: Theme.of(context).textTheme.headlineSmall
                    // ),
                    IgnorePointer(
                      ignoring: widget.settings.timerState == 'running',
                      child: NumberPicker(// Seconds
                        minValue: 0, 
                        maxValue: 60, 
                        value: widget.settings.timerDuration.seconds,
                        step: 1,
                        zeroPad: true,
                        infiniteLoop: true, 
                        haptics: true,
                        onChanged: (int i){
                          if(!widget.isCoolDown){
                            setState(() {
                              widget.settings.timerDuration.seconds = i;
                            });
                            if(widget.settings.timerState != 'running'){
                              widget.mqttController?.publishSetTimerDuration(widget.settings.timerDuration);
                            }
                          }
                        }
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      }
    );
  }
}