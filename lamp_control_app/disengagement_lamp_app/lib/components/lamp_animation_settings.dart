import 'package:flutter/material.dart';



class LampAnimationSettingsMap with ChangeNotifier {
  Map<String, LampAnimationSettings> animationSettings = {};

  void update(String key, LampAnimationSettings settings){
    animationSettings[key] = settings;
    settings.update();
    notifyListeners();
  }
}

class LampAnimationSettings with ChangeNotifier {
  String animPos;
  String animName;
  double duration;
  double blendPoint;
  List<Color> colors;
  int numBlocks;
  List<String> availableAnimations;
  LampAnimationSettings({required this.animPos, required this.animName, required this.duration, required this.colors, required this.blendPoint, required this.numBlocks, required this.availableAnimations});
  
  factory LampAnimationSettings.fromJson(Map<String,dynamic> data, List<String> availableAnimations) {
    List<Color> colors = [];
    if(data.containsKey('color1')) {
      colors.add(Color.fromRGBO(data['color1'][0], data['color1'][1], data['color1'][2], 1.0));
    }
    if(data.containsKey('color2')){
      colors.add(Color.fromRGBO(data['color2'][0], data['color2'][1], data['color2'][2], 1.0));
    }
    
    final settings = LampAnimationSettings(
      animPos:  data['anim_pos'],
      animName:  data['anim_name'],
      duration:  data['duration'].toDouble() ,
      blendPoint: data.containsKey('color1') ? data['blend_point'].toDouble() : 0.5,
      colors: colors,
      numBlocks: data['num_blocks'],
      availableAnimations: availableAnimations
    );
    return settings;
  }


  void update() {
    notifyListeners();
  }
}

// class LampAnimationSettingsWidget extends StatefulWidget {
//   final LampAnimationSettings settings;
//   final MQTTController? mqtt;
//   const LampAnimationSettingsWidget({super.key, required this.settings, required this.mqtt});

//   @override
//   State<LampAnimationSettingsWidget> createState() => _LampAnimationSettingsWidgetState();
// }

// class _LampAnimationSettingsWidgetState extends State<LampAnimationSettingsWidget> {

//   @override
//   Widget build(BuildContext context) {
//     return ListenableBuilder(
//         listenable: widget.settings,
//         builder: (context, builder) => ListView(
//           shrinkWrap: true,
//           physics: const NeverScrollableScrollPhysics(),
//           children: [
//             ListTile(
//               title: Text(widget.settings.animPos),
//               subtitle: DropdownButton<String>(
//                 value: widget.settings.animName,
//                 items: widget.settings.availableAnimations.map<DropdownMenuItem<String>>((String value){
//                   return DropdownMenuItem<String>(
//                     value: value,
//                     child: Text(value),
//                   );
//                 }).toList(), 
//                 onChanged: (String? value) { 
//                   setState(() {
//                     widget.settings.animName = value!;
//                   });
//                   widget.mqtt?.publishLightPlayAnimation(widget.settings);
//                   widget.settings.update();
//                 },
//               ),
//               trailing: IconButton(
//                 icon: const Icon(Icons.play_circle_fill_outlined),
//                 onPressed: (){
//                   widget.mqtt?.publishLightPlayAnimation(widget.settings);
//                 },
//               ),
//             ),
//             ListTile(
//               title: const Text('Duration'),
//               trailing: Text(widget.settings.duration.toStringAsFixed(2)),
//               subtitle: Slider(
//                 value: widget.settings.duration,
//                 min: 0.1,
//                 max: 500,
//                 onChanged: (value) {
//                   setState(() {
//                     widget.settings.duration = value;  
//                   });
//                   widget.mqtt?.publishLightPlayAnimation(widget.settings);
//                 },
//               ),
//             ),
//             ListTile(
//               title: const Text('Blend point'),
//               trailing: Text(widget.settings.blendPoint.toStringAsFixed(2)),
//               subtitle: Slider(
//                 value: widget.settings.blendPoint,
//                 min: 0.0,
//                 max: 1.0,
//                 onChanged: (value){
//                   setState(() {
//                     widget.settings.blendPoint = value;
//                   });
//                   widget.mqtt?.publishLightPlayAnimation(widget.settings);
//                 },
//               ),
//             ),
//             ListTile(
//               title: const Text('# Blocks'),
//               trailing: Text('${widget.settings.numBlocks}'),
//               subtitle: Slider(
//                 value:  widget.settings.numBlocks.toDouble(),
//                 min: 1,
//                 max: 20,
//                 onChanged: (value) {
//                   setState(() {
//                     widget.settings.numBlocks = value.toInt();
//                   });
//                   widget.mqtt?.publishLightPlayAnimation(widget.settings);
//                 },
//               ),
//               ),
//             ...List.generate(widget.settings.colors.length, (index) {
//               return ListTile(
//                 title: Text('Color ${index+1}'),
//                 subtitle: ColorPicker(
//                   color: widget.settings.colors[index],
//                   initialPicker: Picker.rgb, 
//                   onChanged: (color) {
//                     setState(() {
//                       widget.settings.colors[index] = color;
//                     });
//                     // this.widget.mqtt.publishLightSetColorAll(color.red, color.green, color.blue);
//                     widget.mqtt?.publishLightPlayAnimation(widget.settings);
//                   }
//                 ),
//               );
//             }
//           )
//         ],
//       ),
//     );
//   }
// }

