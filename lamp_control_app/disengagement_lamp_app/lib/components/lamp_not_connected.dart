import 'package:flutter/material.dart';

class LampDisconnectedWidget extends StatelessWidget {
  const LampDisconnectedWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Could not connect with the lamp', 
              style: Theme.of(context).textTheme.headlineLarge,)
          ]
        ),
      ),
    );
  }
}