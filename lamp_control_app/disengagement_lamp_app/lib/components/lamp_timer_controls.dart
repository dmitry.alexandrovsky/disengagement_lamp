import 'package:disengagement_lamp_app/components/lamp_timer.dart';
import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:flutter/material.dart';

class LampTimerControlsWidget extends StatelessWidget {
  final MQTTController? mqttController;
  final LampTimerSettings settings;
  final LampTimerWidget timerWidget;
  const LampTimerControlsWidget({
    super.key,
    required this.mqttController, 
    required this.settings,
    required this.timerWidget,
  });

  List<Widget> _buildSnoozeButtons(BuildContext context){
    List<Widget> widgets = [];
    List<TimerDuration> snoozeDurations  = [
      TimerDuration(minutes: 0, seconds:10),
      TimerDuration(minutes: 1, seconds:0), 
      TimerDuration(minutes: 3, seconds:0), 
      TimerDuration(minutes: 5, seconds:0), 
      TimerDuration(minutes: 10, seconds:0), 
      TimerDuration(minutes: 15, seconds:0), 
      TimerDuration(minutes: 30, seconds:0), 
      TimerDuration(minutes: 45, seconds:0), 
      TimerDuration(minutes: 60, seconds:0), 
    ]; 

    for(var snooze in snoozeDurations){
      widgets.add(
        FilledButton.tonal(
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0),
                side: BorderSide(color: Theme.of(context).colorScheme.secondary)
              )
            )
          ),
          onPressed: () {
            timerWidget.startCoolDown();
            if(settings.timerState == 'running'){
              mqttController?.publishTimerModifyDuration(snooze);
            }else{
              mqttController?.publishSetTimerDuration(snooze);
            }
          },
          child: Text(
            '${settings.timerState == 'running'? '+ ' : ''}${snooze.toString()}',
            style: Theme.of(context).textTheme.titleLarge,
            )
        )
      );
      if(settings.timerState == 'running'){
        var negSnooze = TimerDuration(hours: -snooze.hours, minutes: -snooze.minutes, seconds: -snooze.seconds);
        widgets.add(
          FilledButton.tonal(
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(32.0),
                  side: BorderSide(color: Theme.of(context).colorScheme.secondary)
                )
              )
            ),
            onPressed: () {
              timerWidget.startCoolDown();
              if(settings.timerState == 'running'){
                mqttController?.publishTimerModifyDuration(negSnooze);
              }else{
                mqttController?.publishSetTimerDuration(negSnooze);
              }              
            },
            child: Text(
              negSnooze.toString(),
              style: Theme.of(context).textTheme.titleLarge,
            )
          )
        );
      }      
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    const buttonSize = 42.0;
    return ListenableBuilder(
      listenable: settings,
      builder: (context, child) {
        return Expanded(
          flex: 1,
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton.filled(
                      onPressed: settings.timerState == 'running'? null: () {
                        mqttController?.publishTimerStart();
                      }, 
                      iconSize: buttonSize,
                      icon: const Icon(Icons.play_arrow)
                    ),
                    IconButton.filled(
                      onPressed: settings.timerState == 'running'? (){
                        mqttController?.publishTimerPause();
                      }: null,
                      iconSize: buttonSize, 
                      icon: const Icon(Icons.pause)
                    ),
                    IconButton.filled(
                      onPressed: (settings.timerState == 'running') || (settings.timerState == 'paused')? () {
                        mqttController?.publishTimerStop();
                        mqttController?.publishTimerStop();
                      } : null,
                      iconSize: buttonSize,
                      icon: const Icon(Icons.stop)
                    )
                  ],
                ),
              ),
              Text(
                settings.timerState == 'running'? ' Snooze' : 'Presets',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              Expanded(
                flex: 2,
                child: GridView.count(
                    padding: const EdgeInsets.all(16),
                    crossAxisSpacing: 8,
                    mainAxisSpacing: 8,
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    childAspectRatio: 24/9,
                    children: _buildSnoozeButtons(context),
                  ),
              )
              // : Expanded(
              //   flex: 2,
              //   child: Container(),
              // ),
            ],
          ),
        );
      }
    );
  }
}