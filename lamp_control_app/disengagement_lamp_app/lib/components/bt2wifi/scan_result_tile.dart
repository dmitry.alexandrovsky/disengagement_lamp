import 'dart:async';
import 'package:disengagement_lamp_app/services/bluetooth_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';





class ScanResultTile extends StatefulWidget {
  final BluetoothDeviceNetworkInfo? networkInfo;
  const ScanResultTile({Key? key, required this.result, this.onTap, this.networkInfo, required this.showIndicator}) : super(key: key);

  final ScanResult result;
  final VoidCallback? onTap;
  final bool showIndicator;

  @override
  State<ScanResultTile> createState() => _ScanResultTileState();
}

class _ScanResultTileState extends State<ScanResultTile> {
  BluetoothConnectionState _connectionState = BluetoothConnectionState.disconnected;

  late StreamSubscription<BluetoothConnectionState> _connectionStateSubscription;

  @override
  void initState() {
    super.initState();

    _connectionStateSubscription = widget.result.device.connectionState.listen((state) {
      _connectionState = state;
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    _connectionStateSubscription.cancel();
    super.dispose();
  }


  bool get isConnected {
    return _connectionState == BluetoothConnectionState.connected;
  }

  
  @override
  Widget build(BuildContext context) {
    var adv = widget.result.advertisementData;
    return ListTile(
      title: Text(adv.advName),
      leading: Icon(Icons.lightbulb, color: widget.result.device.isConnected? Colors.green : Colors.red,),
      trailing: widget.showIndicator ? const  CircularProgressIndicator(): null,
      subtitle: widget.networkInfo != null 
      ? 
      ListenableBuilder(listenable: widget.networkInfo!, builder: 
        (context, child){
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('WiFi name: ${widget.networkInfo!.ssid}'),
              Text('IP address: ${widget.networkInfo!.ipAddress}'),
            ]
          );
        }
      )
      : null,
      onTap: widget.onTap,
    );
  }
}
