import 'dart:async';

import 'package:disengagement_lamp_app/components/bt2wifi/bt_network_dialog.dart';
import 'package:disengagement_lamp_app/components/bt2wifi/scan_result_tile.dart';
import 'package:disengagement_lamp_app/components/bt2wifi/system_device_tile.dart';
import 'package:disengagement_lamp_app/pages/init_setup_page.dart';
import 'package:disengagement_lamp_app/services/app_preferences.dart';
import 'package:disengagement_lamp_app/services/bluetooth_controller.dart';
import 'package:disengagement_lamp_app/utils/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BluetoothScan extends StatefulWidget {
  final BluetoothController? bluetoothController;
  final Function(String,String) onBluetoothWifiSet;
  const BluetoothScan({super.key, required this.onBluetoothWifiSet, required this.bluetoothController});


  @override
  State<BluetoothScan> createState() => _BluetoothScanState();
}

class _BluetoothScanState extends State<BluetoothScan> {
  List<BluetoothDevice> _systemDevices = [];
  final List<ScanResult> _scanResults = [];
  bool _isScanning = false;
  late StreamSubscription<List<ScanResult>> _scanResultsSubscription;
  late StreamSubscription<bool> _isScanningSubscription;

  List<String> characteristics = [];

  bool _isConnecting = false;
  final AppNetworkInfo _networkInfo = AppNetworkInfo();

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((sp) {
      setState(() {
        _networkInfo.wifiIP = sp.getString(AppPreferences.APP_DEVICE_IP_ADDRESS) ?? '';
        _networkInfo.wifiName = sp.getString(AppPreferences.APP_DEVICE_SSID)?? 'NONE';  
      });
    });
    
    _scanResultsSubscription = FlutterBluePlus.scanResults.listen((results) {
      _scanResults.clear();
      print('# results: ${results.length}');
      for(var res in results){
        if(res.device.advName == 'Schlusslicht'){
          _scanResults.add(res);
        }
        // _scanResults.add(res);
      }
      // _scanResults = results;
      // print(_scanResults);
        if (mounted) {
          setState(() {});
        }
      }, onError: (e) {
        Snackbar.show(ABC.b, prettyException("Scan Error:", e), success: false);
    });

    _isScanningSubscription = FlutterBluePlus.isScanning.listen((state) {
      _isScanning = state;
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    _scanResultsSubscription.cancel();
    _isScanningSubscription.cancel();
    super.dispose();
  }

  Future onScanPressed() async {
    try {
      _systemDevices = await FlutterBluePlus.systemDevices;
      for(var device in _systemDevices){
        print(device);
      }
    } catch (e) {
      Snackbar.show(ABC.b, prettyException("System Devices Error:", e), success: false);
    }
    try {
      await FlutterBluePlus.startScan(timeout: const Duration(seconds: 15));
    } catch (e) {
      Snackbar.show(ABC.b, prettyException("Start Scan Error:", e), success: false);
    }
    if (mounted) {
      setState(() {});
    }
  }

  Future onStopPressed() async {
    try {
      FlutterBluePlus.stopScan();
    } catch (e) {
      Snackbar.show(ABC.b, prettyException("Stop Scan Error:", e), success: false);
    }
  }

  
  void openNetworkDataDialog() {
    setState(() {
      _isConnecting = false;  
    });
    
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      
      return BluetoothNetworkDialog(
        bluetoothController: widget.bluetoothController,
        appNetworkInfo: _networkInfo,
      );
    }));
    return;
    // showDialog(context: context, 
    //       builder: (context) {
    //         bool obscured = true;
    //         return AlertDialog(
    //           title: Text("Connect ${widget.bluetoothController?.device?.advName} in your Wifi"),
    //           content: Column(
    //             crossAxisAlignment: CrossAxisAlignment.start,
    //             children: [
    //             const Text('SSID'),
    //             TextField(
    //               autocorrect: false,
    //               controller: _ssidController,
    //               // onSubmitted: (String ssid){},
    //             ),
    //             const Text('Password'),
    //             TextField(
    //               obscureText: obscured,
    //               autocorrect: false,
    //               controller: _passwordController,
    //               decoration: InputDecoration(
    //                 floatingLabelBehavior: FloatingLabelBehavior.never,
    //                 // labelText: "TEST",
    //                 // isDense: true,
    //                 // border: OutlineInputBorder(
    //                 //   borderSide: BorderSide.none,
    //                 //   borderRadius: BorderRadius.circular(8)
    //                 // ),
    //                 // prefixIcon: const Icon(Icons.lock_clock_rounded),
    //                 suffix: GestureDetector(
    //                   onTap: () {
    //                     setState(() {
    //                       print("$obscured");
    //                       obscured = !obscured;  
    //                     });
    //                   },
    //                   child: Icon(
    //                     obscured == true
    //                   ? Icons.visibility_rounded
    //                   : Icons.visibility_off_rounded,
    //                   ),
    //                 )
                    
    //               )
    //               // onSubmitted: (String password){},
    //             )
    //           ]),
    //           actions: [
    //             TextButton(
    //               onPressed: (){
    //                 Navigator.pop(context);
    //               },
    //               child: const Text('Cancel')
    //             ),
    //             TextButton(
    //               onPressed: () {
    //                 widget.bluetoothController?.writeSSIDandPassword(_ssidController.text, _passwordController.text).then((value) {
    //                   Snackbar.show(ABC.b, "Lamp network set", success: true);
    //                   Navigator.pop(context);
                      
    //                 });
    //               },
    //               child: const Text('Submit')
    //             )
    //           ],
    //         );
    //       }
    //     );
  }

  void onConnectPressed(BluetoothDevice device) async {
    
    setState(() {
      _isConnecting = true;  
    });
    try {
      await FlutterBluePlus.stopScan();
      if(device.isDisconnected){
        await widget.bluetoothController?.connectAndInitialize(device).then((value) {
          openNetworkDataDialog();
        });
      }else {
        openNetworkDataDialog();
      }
    } catch (e) {
      Snackbar.show(ABC.c, prettyException("Connect Error:", e), success: false);
    }
  }

  Future onRefresh() {
    if (_isScanning == false) {
      FlutterBluePlus.startScan(timeout: const Duration(seconds: 5));
    }
    if (mounted) {
      setState(() {});
    }
    
    

    return Future.doWhile(() => !FlutterBluePlus.isScanningNow);

    // return Future.delayed(const Duration(milliseconds: 50));
    
  }

  Widget buildScanButton(BuildContext context) {
    
      return IconButton.filled(
        icon: FlutterBluePlus.isScanningNow? const Icon(Icons.stop) : const Icon(Icons.search),
        onPressed: FlutterBluePlus.isScanningNow? onStopPressed: onScanPressed,
      );
    
  }

  List<Widget> _buildSystemDeviceTiles(BuildContext context) {
    return _systemDevices
        .map(
          (d) => SystemDeviceTile(
            device: d,
            onOpen: (){
              print('open!!!');
            },
            // () => Navigator.of(context).push(
            //   MaterialPageRoute(
            //     builder: (context) => DeviceScreen(device: d),
            //     settings: RouteSettings(name: '/DeviceScreen'),
            //   ),
            // ),
            onConnect: () => onConnectPressed(d),
          ),
        )
        .toList();
  }

  List<Widget> _buildScanResultTiles(BuildContext context) {
    return _scanResults.map(
      (r) {
        bool showIndicator = false;
        BluetoothDevice device = r.device;
        BluetoothDeviceNetworkInfo? networkInfo;
        if(r.device == widget.bluetoothController?.device){
          device = widget.bluetoothController!.device!;
          networkInfo = widget.bluetoothController?.networkInfo;
          showIndicator = _isConnecting;
        }
        
        return ScanResultTile(
          result: r,
          networkInfo: networkInfo,
          onTap: () => onConnectPressed(device),
          showIndicator: showIndicator,
        );
      } 
    )
    .toList();
  }

  @override
  Widget build(BuildContext context) {
    return 
    Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text('Connect the app with the lamp device via bluetooth'),
        Expanded(
          flex: 1,
          // child: RefreshIndicator(
            // onRefresh: onRefresh,
            child: ListView(
              shrinkWrap: true,
              // physics: const NeverScrollableScrollPhysics(),
              children: [
                ..._buildScanResultTiles(context),
              ],
            ),
          // ),
        ),
        Text("Network: ${_networkInfo.wifiName}",),
        buildScanButton(context),
      ],
    );
  }
}