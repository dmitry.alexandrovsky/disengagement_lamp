import 'package:disengagement_lamp_app/pages/init_setup_page.dart';
import 'package:disengagement_lamp_app/services/bluetooth_controller.dart';
import 'package:flutter/material.dart';
import 'package:markup_text/markup_text.dart';

class BluetoothNetworkDialog extends StatefulWidget {
  const BluetoothNetworkDialog({super.key, this.bluetoothController, required this.appNetworkInfo});
  final BluetoothController? bluetoothController;
  final AppNetworkInfo appNetworkInfo;
  @override
  State<BluetoothNetworkDialog> createState() => _BluetoothNetworkDialogState();
}

class _BluetoothNetworkDialogState extends State<BluetoothNetworkDialog> {

  bool _obscuredPassword = true;

  late TextEditingController _ssidController;
  late TextEditingController _passwordController;
  
  @override
  void initState() {
    super.initState();
    _ssidController = TextEditingController(text: widget.appNetworkInfo.wifiName);
    _passwordController = TextEditingController();
  }


  @override
  void dispose() {
    _ssidController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Network setup"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Set the network name (SSID) and password of the target network.'),
              const Text('Please make sure the app is in the same WiFi as the lamp.'),
              const Divider(),
              const Spacer(),
              const Text('SSID'),
              TextField(
                  autocorrect: false,
                  controller: _ssidController,
                  // onSubmitted: (String ssid){},
                ),
                const Text('Password'),
                TextField(
                  obscureText: _obscuredPassword,
                  autocorrect: false,
                  controller: _passwordController,
                  decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    suffix: GestureDetector(
                      onTap: () {
                        setState(() {
                          print("obscured: $_obscuredPassword");
                          _obscuredPassword = !_obscuredPassword;  
                        });
                      },
                      child: Icon(
                        _obscuredPassword == true
                      ? Icons.visibility_rounded
                      : Icons.visibility_off_rounded,
                      ),
                    )
                  )
                  // onSubmitted: (String password){},
                ),
                const Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    FilledButton(onPressed: () {
                        Navigator.pop(context);
                      }, child: const Text('Cancel')
                    ),
                    FilledButton(onPressed: () {
                      widget.bluetoothController?.writeSSIDandPassword(_ssidController.text, _passwordController.text).then((value) {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Lamp network set")));
                      Navigator.pop(context);
                      
                    });
                    }, child: const Text("Submit"))
                ],
              ),
              MarkupText(
                'This device is connected to the network:\n(b)${widget.appNetworkInfo.wifiName}(/b)',
                style: Theme.of(context).textTheme.bodySmall,
              )
            ]),
        ),
      );
  }
}