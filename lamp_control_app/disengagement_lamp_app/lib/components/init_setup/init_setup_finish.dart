import 'package:disengagement_lamp_app/services/mqtt_controller.dart';
import 'package:flutter/material.dart';

class InitSetupFinish extends StatelessWidget {
  const InitSetupFinish({super.key, required this.mqttController});
  final MQTTController? mqttController;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text('All setup complete'),
          FilledButton(
            onPressed: mqttController != null && mqttController!.isConnected() ? () {
              Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
            }
            : null, 
            child: const Text('Continue'))
        ]),
    );
  }
}