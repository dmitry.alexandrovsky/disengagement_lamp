import 'package:flutter/material.dart';

class InitSetupWelcome extends StatelessWidget {
  const InitSetupWelcome({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text('Welcome',
          style: Theme.of(context).textTheme.headlineMedium,),
          // const Spacer(),
          Text('Could not connect to the tangible.',
          style: Theme.of(context).textTheme.bodyLarge,
          ),
          // const Spacer(),
          Text('Please make sure the phone and the lamp are in the same Wifi',
          style: Theme.of(context).textTheme.bodyLarge,
          ),
          Text('Please follow the setup instructions.',
          style: Theme.of(context).textTheme.bodyLarge,),
         
        ]
      ),
    );
  }
}