# disengagement_lamp "Schlusslicht"

## Pi - Init project
 
1. Note that this project contains (nested) submodules.
Run `git clone https://YOURUSERNAME:YOURACCESSTOKEN@gitlab.kit.edu/dmitry.alexandrovsky/disengagement_lamp.git --recurse-submodules` to include them directly, or `git submodule update --init --recursive` after cloning. 
2. Follow the steps of section "Pi - Enable SPI"
3. Follow the steps of section "Pi - Install python packages"
4. Follow the steps of section "Pi - MQTT Broker setup"
5. Follow the steps of section "Pi - Add main backend as service"
6. Follow the steps of section "Pi - Add bt2wifi backend as service"
 

To fetch submodul changes, run `git fetch`, `git pull origin master` and `git submodule update --init --recursive` inside the submodul folders.

# Network config
Router pw: lamp

WiFi pw: lamplamplamp

Pi IP: 192.168.1.xxx

# Communication encoding

Communication directions:\
<-: clients to Pi,\
->: Pi to clients



| topic | message type | direction | keys / details |
| ------ | ------ | ------ | ------ |
|timer|||
|timer/set_duration|JSON str|<-|duration (\[s], float)|
|timer/modify_duration|JSON str|<-|delta (\[s], float, added to duration, can be called in runtime, snooze: positive delta,  subtract time: negative delta)|
|timer/start|/|<-|/|
|timer/pause|/|<-|/|
|timer/stop|/|<-|/|
|timer/events|JSON str|->|event (idle, running, paused, stopped, ended, modify_duration), data (None, JSON str for modify_duration containing delta, remaining_duration, total_duration, timer_state)|
||||
|light|||
|light/set_color_all|JSON str|<-|color (int tupel or array of rgb values (0-255))|
|light/set_brightness|JSON str|<-|brightness (float, 0-1)|
|light/req_brightness|/|<-|leads to an answer on channel light/brightness_info|
|light/brightness_info|JSON str|->|brightness (float, 0-1)|
|light/set_animation|JSON str|<-|anim_pos (idle, pre_start, post_start, running, paused, pre_end, post_end), anim_name (according to options, for example "linear_increase"), duration \[s], color1 (optional, comma-separated: r, g, b values (0-255)), color2(optional, comma-separated: r, g, b values (0-255), will init a linear blend between color1 and color2 along the leds), pre_end_threshold (optional, 0-1, init point for pre_end animation), num_blocks (number of blocks for blockwise decay/grow)|
|light/play_animation|JSON str|<-|see light/set_animation, played once|
|light/req_anim_info|/|<-|leads to an answer on channel light/anim_info|
|light/anim_info|JSON str|->|available_positions (all anim pos), available_animations (all anim names), several keys [anim pos] (JSON dump of anim options for this anim pos)
|light/info|JSON str|->|remaining duration, total duration, action state (idle/running/paused/ended), current animation (idle, pre_start, post_start, running, paused, pre_end, post_end), current animation frame, total animation frames|

## JSON string examples

Example JSON str to set a duration:
```
{
    "duration": 30.0
}
```

Example JSON str to modify a duration:
```
{
    "delta": -10.0
}
```

Example JSON str of an event:
```
{
    "event": "ended"
}
```

Example JSON str to set all colors:
```
{
    "color": (30, 0, 255)
}
```

Example JSON str to set brightness or receive brightness info:
```
{
    "brightness": 0.2
}
```

Example JSON str of timer/events 1:
```
{
    "event": "stopped",
    "data": None
}
```

Example JSON str of timer/events 2:
```
{
   "event": "modify_duration",
   "data": {
      "delta": -10.0,
      "remaining_duration": 28.0,
      "total_duration": 30.0,
      "timer_state": "running"
   }
}
```

Example JSON str to set or play an animation:
```
{
    "anim_pos": "idle",
    "anim_name": "Constant View",
    "duration": 1.0,
    "color1": (100, 100, 0),
    "color2": (0, 255, 0),
    "blend_point": 0.0,
    "num_blocks": 1
}
```

Example JSON str of light/anim_info channel:
```
{
    "available_positions": "idle, pre_start, post_start, running, paused, pre_end, post_end, showcase",
    "available_animations": "Pulsation, Constant View, Increasing Line, Decreasing Line, Increasing Blocks, Decreasing Blocks, Increasing Middle Line",
    "idle": {
        "anim_pos": "idle",
        "anim_name": "Constant View",
        "duration": 1,
        "color1": [100, 100, 0],
        "color2": [0, 255, 0],
        "blend_point": 0,
        "num_blocks": 1
    },
    "pre_start": {
        "anim_pos": "pre_start",
        "anim_name": "Increasing Middle Line",
        "duration": 2,
        "color1": [0, 255, 0],
        "color2": [0, 255, 0],
        "blend_point": 0,
        "num_blocks": 1
    },
    "post_start": {
        "anim_pos": "post_start",
        "anim_name": "Pulsation",
        "duration": 2,
        "num_blocks": 1
    },
    "running": {
        "anim_pos": "running",
        "anim_name": "Decreasing Line",
        "duration": 144,
        "color1": [255, 0, 0],
        "color2": [0, 255, 0],
        "blend_point": 1,
        "num_blocks": 1
    },
    "paused": {
        "anim_pos": "paused",
        "anim_name": "Pulsation",
        "duration": 2,
        "color1": [100, 100, 0],
        "color2": [0, 255, 0],
        "blend_point": 0,
        "num_blocks": 1
    },
    "pre_end": {
        "anim_pos": "pre_end",
        "anim_name": "Pulsation",
        "duration": 2,
        "num_blocks": 1
    },
    "post_end": {
        "anim_pos": "post_end",
        "anim_name": "Pulsation",
        "duration": 2,
        "color1": [255, 0, 0],
        "color2": [0, 255, 0],
        "blend_point": 0,
        "num_blocks": 1
    },
    "showcase": null
}
```

Example JSON str of light/info channel:
```
{
    "remaining_duration": 15.0,
    "total_duration": 15.0,
    "timer_state": "idle",
    "current_animation": "idle",
    "animation_name": "Constant View",
    "current_frame": 10,
    "num_frames": 30
}
```

## Pi - Enable SPI

1. `sudo raspi-config`
2. This will launch the raspi-config utility. Select “Interfacing Options”
3. Highlight the “SPI” option and activate “<Select>”
4. Select and activate “<Yes>”
5. Highlight and activate “<Ok>”
6. When prompted to reboot highlight and activate “<Yes>” 
7. The Raspberry Pi will reboot and the interface will be enabled.


## Pi - Install python packages

1. `pip3 install --force-reinstall -v paho-mqtt==1.6.1 --break-system-packages`
2. `pip3 install --force-reinstall -v adafruit-circuitpython-dotstar --break-system-packages`


## Pi - MQTT Broker setup 

1. `sudo apt install -y mosquitto mosquitto-clients`
2. `sudo systemctl enable mosquitto.service`
3. `sudo nano /etc/mosquitto/mosquitto.conf`
4. Move to the end of the file using the arrow keys and paste the following two lines:
```
listener 1883
allow_anonymous true
```

7. Then, press CTRL-X to exit and save the file. Press Y and Enter.
8. `sudo systemctl restart mosquitto`

## Pi - Add main backend as service
1. `cd /lib/systemd/system/`
2. `sudo nano lamp.service`
3. Add following information to the file:
```
[Unit]
Description=Lamp Service
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=lamp
WorkingDirectory=/home/lamp/disengagement_lamp/lamp_control_pi
ExecStart=/usr/bin/python /home/lamp/disengagement_lamp/lamp_control_pi/main.py
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```
4. Save and exit file (ctrl + x -> shift + y -> enter)
5. `sudo chmod 644 /lib/systemd/system/lamp.service`
6. `chmod +x /home/lamp/disengagement_lamp/lamp_control_pi/main.py`
7. `sudo systemctl daemon-reload`
8. `sudo systemctl enable lamp.service`
9. `sudo systemctl start lamp.service`

You can check the status via:
`sudo systemctl status lamp.service`
and the output via:
`sudo journalctl -f -u lamp.service`

After changes to the code you need to refresh the service:
1. `sudo systemctl stop lamp.service`
2. `sudo systemctl daemon-reload`
3. `sudo systemctl start lamp.service`

## Pi - Start the lamp backend manually 
**Not required when backend is running as service!**

1. connect via SSH to the raspberry pi
`ssh lamp@pi.local`
2. navigate to the backend
`cd ~/lamp_control_pi/`
3. start the backend application
`python3 main.py`

## Pi - Add bt2wifi backend as service
1. `cd /lib/systemd/system/`
2. `sudo nano bt2wifi.service`
3. Add following information to the file:
```
[Unit]
Description=BT2WiFi Service
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=lamp
WorkingDirectory=/home/lamp/disengagement_lamp/lamp_control_pi/bt2wifi
ExecStart=/usr/bin/python /home/lamp/disengagement_lamp/lamp_control_pi/bt2wifi/main.py -name Schlusslicht
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```
4. Save and exit file (ctrl + x -> shift + y -> enter)
5. `sudo chmod 644 /lib/systemd/system/bt2wifi.service`
6. `chmod +x /home/lamp/disengagement_lamp/lamp_control_pi/bt2wifi/main.py`
7. `sudo systemctl daemon-reload`
8. `sudo systemctl enable bt2wifi.service`
9. `sudo systemctl start bt2wifi.service`

You can check the status via:
`sudo systemctl status bt2wifi.service`
and the output via:
`sudo journalctl -f -u bt2wifi.service`

After changes to the code you need to refresh the service:
1. `sudo systemctl stop bt2wifi.service`
2. `sudo systemctl daemon-reload`
3. `sudo systemctl start bt2wifi.service`

# How to cite this material
DOI: 10.5281/zenodo.10868729
